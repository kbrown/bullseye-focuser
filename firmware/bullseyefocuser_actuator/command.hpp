/*******************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef COMMAND_H
#define COMMAND_H

#include <stdlib.h>
#include <stdint.h>

#include "callbackbase.hpp"

class HoHoo;

#define SB      0xD2
#define ML_MIN  0x06
#define ML_MAX  0x0A
#define EB      0xF0
#define HDR_LEN 0x04

namespace CommandUtils
{
    template <typename T>
    void toBytes(uint8_t *buf, const T &param, uint8_t &cs);

    template <>
    void toBytes<float>(uint8_t *buf, const float &param, uint8_t &cs);



    template <typename T>
    void fromBytes(const uint8_t *buf, T &param);

    template <>
    void fromBytes<float>(const uint8_t *buf, float &param);
}

/*!
\brief Command base class
*/
class Command
{
public:
    Command(HoHoo *parent);

    void        unRegisterCommand();

    template <typename T>
    bool        registerCommand(const uint8_t &cmd, const uint8_t &param_type, CallbackBase<T> *callback);
    bool        registerCommand(const uint8_t &cmd, const uint8_t &param_type);

    /*! \brief Returns the type of the parameter of this command. */
    const uint8_t&     getParamType()
    {
        return m_param_type;
    }

    template <typename T>
    void        asStream(uint8_t *buf, const T &param, uint8_t &message_len);
    void        asStream(uint8_t *buf, uint8_t &message_len);

    void        run(const uint8_t *buf);

    void        send();
    template <typename T>
    void        send(const T &param);

    /*! \brief Comparison operator overload. */
    bool        operator==(const uint8_t &b)
    {
        return this->m_cmd == b;
    }

private:
    /*! \brief Pointer to an instance of class HoHoo. */
    HoHoo       *m_parent;

    /*! \brief Command number of this Command instance. */
    uint8_t     m_cmd;
    /*! \brief Parameter type of this command. */
    uint8_t     m_param_type;

    void        initCommand(const uint8_t &cmd, const uint8_t &param_type);
    uint8_t     initBuffer(uint8_t *buf, const uint8_t &ml);

    /*! \brief Pointer to the callback function of this command. */
    void        *m_callback;
};

/*!
\brief Registers this command.

This version is for commands with a callback.

@param cmd The command number.
@param param_type Data type of the command's parameter.
@param callback Pointer to an instance of class CallbackBase.

\return true if succesful
\return false if not succesful
*/
template <typename T>
bool Command::registerCommand(const uint8_t &cmd, const uint8_t &param_type, CallbackBase<T> *callback)
{
    if(m_cmd != 0xFF)
        return false;

    initCommand(cmd, param_type);
    
    m_callback = callback;

    return true;        
}

/*!
\brief Prepares buffer for sending.

This version is for commands with a parameter of type T.

@param buf Pointer to the buffer.
@param param The parameter to the command.
@param message_len Message length.
*/
template <typename T>
void Command::asStream(uint8_t *buf, const T &param, uint8_t &message_len)
{
    const uint8_t ml = sizeof(param) + 6;
    uint8_t cs = initBuffer(buf, ml);
    CommandUtils::toBytes(&buf[4], param, cs);
    buf[ml - 1] = cs;

    message_len = ml;
}
#endif