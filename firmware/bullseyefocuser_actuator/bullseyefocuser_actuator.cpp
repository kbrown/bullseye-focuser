/*****************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*****************************************************************************/

#include "bullseyefocuser_actuator.hpp"

void(*resetFunc)(void) = 0; //declare reset function @ address 0

volatile uint32_t   BullsEyeFocuserActuator::s_idxCount = 0;
volatile bool       BullsEyeFocuserActuator::s_diagEnable = false;
volatile BullsEyeFocuserActuator *BullsEyeFocuserActuator::s_bef;

void BullsEyeFocuserActuator::diagInterrupt();
void BullsEyeFocuserActuator::idxInterrupt();

/*! \brief Constructor

Initialises I/O pins and internal member classes and variables. Configures interrupt functions. Sets up serial communications and registers commands. Loads user settings from EEPROM.

@param send_callback The callback function to handle communication sends.
*/
BullsEyeFocuserActuator::BullsEyeFocuserActuator(void(*send_callback)(const uint8_t &count, const uint8_t *buf)) :
    AccelStepper(AccelStepper::DRIVER, STEP_PIN, DIR_PIN),
    TMC2209Stepper(SW_RX, SW_TX, R_SENSE, DRIVER_ADDRESS),
    m_runLevel(RunLevel::RUN_INIT),
    m_lastUpdate(0),
    m_maxStepSpeed(STEP_SPEED),
    m_acceleration(ACCELERATION),
    m_microSteps(MICROSTEPS),
    m_maxPosition(0x7FFFFFFF),
    m_curTarget(0),
    m_temperature(-273.15),
    m_minLimit(1),
    m_lastMinLimit(0),
    m_maxLimit(0),
    m_lastMaxLimit(1),
    m_driverCommsStatus(-1),
    m_timeoutCount(0),
    m_ahStatus(AutoHome::AH_INIT),
    m_curOverShoot(0),
    m_rmsCurrent(RMS_CURRENT),
    m_homingSpeed(HOMING_SPEED),
    m_homingStallOutValue(STALL_VALUE),
    m_homingStallInValue(STALL_VALUE),
    m_homingTimeout(HOMING_TIMEOUT),
    m_overShoot(OVERSHOOT),
    m_flags(0b00000000)
{
    #if FASTADC
        // set prescale to 16 for faster (1 MHz / 77kHz) ADC
        sbi(ADCSRA,ADPS2);
        cbi(ADCSRA,ADPS1);
        cbi(ADCSRA,ADPS0);
    #endif

    pinMode(EN_PIN, OUTPUT);
    digitalWrite(EN_PIN, LOW);
    pinMode(DIR_PIN, OUTPUT);
    digitalWrite(DIR_PIN, LOW);
    pinMode(STEP_PIN, OUTPUT);
    digitalWrite(STEP_PIN, LOW);
    pinMode(VIO_PIN, OUTPUT);
    digitalWrite(VIO_PIN, LOW);
    pinMode(LEDRED_PIN, OUTPUT);
    digitalWrite(LEDRED_PIN, HIGH);     // RED, HIGH = OFF
    pinMode(LEDGRN_PIN, OUTPUT);
    digitalWrite(LEDGRN_PIN, HIGH);     // GREEN, HIGH = OFF

    pinMode(DIAG_PIN, INPUT);
    pinMode(IDX_PIN, INPUT);

    s_bef = this;

    attachInterrupt(digitalPinToInterrupt(DIAG_PIN), BullsEyeFocuserActuator::diagInterrupt, RISING);
    attachInterrupt(digitalPinToInterrupt(IDX_PIN), BullsEyeFocuserActuator::idxInterrupt, RISING);

    AccelStepper::setEnablePin(EN_PIN);
    AccelStepper::setPinsInverted(false, false, true);

    cb_stop = new CB_stop(this);
    cb_goToTarget = new CB_goToTarget(this);
    cb_reset = new CB_reset(this);
    cb_engage = new CB_engage(this);

    cb_getDeviceId = new CB_getDeviceId(this);
    cb_getSettings = new CB_getSettings(this);
    cb_getUpdate = new CB_getUpdate(this);

    cb_error = new CB_error(this);
    cb_setPos = new CB_setPos(this);
    cb_setNewTarget = new CB_setNewTarget(this);
    cb_setMicroSteps = new CB_setMicroSteps(this);
    cb_setMaxPosition = new CB_setMaxPosition(this);
    cb_setStepSpeed = new CB_setStepSpeed(this);
    cb_setAcceleration = new CB_setAcceleration(this);
    cb_setHomingSpeed = new CB_setHomingSpeed(this);
    cb_setHomingStallValues = new CB_setHomingStallValues(this);
    cb_setHomingTimeout = new CB_setHomingTimeout(this);
    cb_setOverShoot = new CB_setOverShoot(this);
    cb_setRmsCurrent = new CB_setRmsCurrent(this);
    cb_setFlags = new CB_setFlags(this);

    m_oneWire = new OneWire(OW_PIN);
    m_dallasTemperature = new DallasTemperature(m_oneWire);
    m_dallasTemperature->begin(); 

    registerCommands();
    m_usbComms.setSendCallback(send_callback);
    usb_cmd_retRunLevel->send(m_runLevel);

    loadSettings();
}

/*! \brief Destructor

Deletes all allocated instances of the callback classes.
*/  
BullsEyeFocuserActuator::~BullsEyeFocuserActuator()
{
    // Delete callback objects (commands are deleted automatically)

    delete cb_stop;
    delete cb_goToTarget;
    delete cb_reset;
    delete cb_engage;

    delete cb_getDeviceId;
    delete cb_getSettings;
    delete cb_getUpdate;

    delete cb_error;
    delete cb_setPos;
    delete cb_setNewTarget;
    delete cb_setMicroSteps;
    delete cb_setMaxPosition;
    delete cb_setStepSpeed;
    delete cb_setAcceleration;
    delete cb_setHomingSpeed;
    delete cb_setHomingStallValues;
    delete cb_setHomingTimeout;
    delete cb_setOverShoot;
    delete cb_setRmsCurrent;
    delete cb_setFlags;

    delete m_dallasTemperature;
    delete m_oneWire;
}

/*! \brief TMC diagnostics interrupt routine.

This function is used for reacting to stall detection events at auto home run level and also to report stall at normal run level.
*/
void BullsEyeFocuserActuator::diagInterrupt()
{
    if(s_diagEnable)
    {
        if(s_bef->m_ahStatus == AutoHome::AH_RUN_OUT)
        {
            s_bef->TMC2209Stepper::VACTUAL(0);
            delay(100);
            s_idxCount = 0;
            s_bef->TMC2209Stepper::SGTHRS(s_bef->m_homingStallInValue);
            s_bef->m_ahStatus = AutoHome::AH_RUN_IN;
            s_diagEnable = false;
            return;
        }

        if(s_bef->m_ahStatus == AutoHome::AH_RUN_IN)
        {
            s_bef->TMC2209Stepper::VACTUAL(0);
            delay(100);
            s_bef->m_absoluteMaxPosition = 2 * s_idxCount;
            s_bef->m_homingMarigin = (float)(s_bef->m_absoluteMaxPosition) * 0.005;
            s_bef->setMaxPosition(s_bef->m_absoluteMaxPosition - 2 * s_bef->m_homingMarigin - s_bef->m_overShoot);
            s_bef->AccelStepper::move(s_bef->m_homingMarigin);
            if(!(s_bef->m_flags & FLAG_USEACCEL))
                s_bef->AccelStepper::setSpeed(s_bef->m_maxStepSpeed);
            s_bef->m_ahStatus = AutoHome::AH_RUN_HOME;
            s_diagEnable = false;
            return;
        }

        if(s_bef->m_runLevel == RunLevel::RUN_NORMAL)
        {
            s_bef->usb_cmd_retTmcDiag->send();
        }
    }
}

/*! \brief TMC index interrupt routine.
This routine simply counts microsteps. It is used by the auto homing mechanism to ignore a certain amount of steps and to measure the length of travel.
*/
void BullsEyeFocuserActuator::idxInterrupt()
{
    s_idxCount++;
}

/*! \brief Loads user settings from EEPROM. */
void BullsEyeFocuserActuator::loadSettings()
{
    Settings settings;
    EEPROM.readBlock(0, settings);
    uint8_t cs = settings.deviceId +
                 settings.rmsCurrent +
                 settings.maxPosition +
                 settings.maxStepSpeed +
                 settings.acceleration +
                 settings.microSteps +
                 settings.homingSpeed +
                 settings.homingStallValues +
                 settings.homingTimeout +
                 settings.overShoot +
                 settings.flags;
    if(settings.deviceId == DEVICE_ID && cs == settings.checksum)
    {
        setRmsCurrent(settings.rmsCurrent);
        setMaxPosition(settings.maxPosition);
        setStepSpeed(settings.maxStepSpeed);
        setAcceleration(settings.acceleration);
        setMicroSteps(settings.microSteps);
        setHomingSpeed(settings.homingSpeed);
        setHomingStallValues(settings.homingStallValues);
        setHomingTimeout(settings.homingTimeout);
        setOverShoot(settings.overShoot);
        setFlags(settings.flags);
    }
    else
        saveSettings();
}

/*! \brief Saves user settings to EEPROM */
void BullsEyeFocuserActuator::saveSettings()
{
    Settings settings;
    settings.deviceId = DEVICE_ID;
    settings.rmsCurrent = m_rmsCurrent;
    settings.maxPosition = m_maxPosition;
    settings.maxStepSpeed = m_maxStepSpeed;
    settings.acceleration = m_acceleration;
    settings.microSteps = m_microSteps;
    settings.homingSpeed = m_homingSpeed;
    settings.homingStallValues = getHomingStallValues();
    settings.homingTimeout = m_homingTimeout;
    settings.overShoot = m_overShoot;
    settings.flags = m_flags;

    settings.checksum = settings.deviceId +
                          settings.rmsCurrent +
                          settings.maxPosition +
                          settings.maxStepSpeed +
                          settings.acceleration +
                          settings.microSteps +
                          settings.homingSpeed +
                          settings.homingStallValues +
                          settings.homingTimeout +
                          settings.overShoot +
                          settings.flags;
                          
    EEPROM.writeBlock(0, settings);
}

/*! \brief Registers all the serial communication commands with the HoHoo communication system. */
void BullsEyeFocuserActuator::registerCommands()
{
    // Actions
    usb_cmd_stop = m_usbComms.registerCommand(stop_C, stop_T, cb_stop);
    usb_cmd_goToTarget = m_usbComms.registerCommand(goToTarget_C, goToTarget_T, cb_goToTarget);
    usb_cmd_reset = m_usbComms.registerCommand(reset_C, reset_T, cb_reset);
    usb_cmd_engage = m_usbComms.registerCommand(engage_C, engage_T, cb_engage);

    // Requests / Settings (USB to Focuser)
    usb_cmd_getDeviceId = m_usbComms.registerCommand(getDeviceId_C, getDeviceId_T, cb_getDeviceId);
    usb_cmd_getSettings = m_usbComms.registerCommand(getSettings_C, getSettings_T, cb_getSettings);

    // Requests / Periodic (USB to Focuser)
    usb_cmd_getUpdate = m_usbComms.registerCommand(getUpdate_C, getUpdate_T, cb_getUpdate);

    // Responses (Focuser to USB)
    usb_cmd_error = m_usbComms.registerCommand(error_C, error_T, cb_error);
    usb_cmd_retDeviceId = m_usbComms.registerCommand(retDeviceId_C, retDeviceId_T);
    usb_cmd_retCurPosition = m_usbComms.registerCommand(retCurPosition_C, retCurPosition_T);
    usb_cmd_retCurTarget = m_usbComms.registerCommand(retCurTarget_C, retCurTarget_T);
    usb_cmd_retMotorStatus = m_usbComms.registerCommand(retMotorStatus_C, retMotorStatus_T);
    usb_cmd_retMicroSteps = m_usbComms.registerCommand(retMicroSteps_C, retMicroSteps_T);
    usb_cmd_retMaxPosition = m_usbComms.registerCommand(retMaxPosition_C, retMaxPosition_T);
    usb_cmd_retStepSpeed = m_usbComms.registerCommand(retStepSpeed_C, retStepSpeed_T);
    usb_cmd_retTemperature = m_usbComms.registerCommand(retTemperature_C, retTemperature_T);
    usb_cmd_minLimit = m_usbComms.registerCommand(minLimit_C, minLimit_T);
    usb_cmd_maxLimit = m_usbComms.registerCommand(maxLimit_C, maxLimit_T);
    usb_cmd_retDriverStatus = m_usbComms.registerCommand(retDriverStatus_C, retDriverStatus_T);
    usb_cmd_retAcceleration = m_usbComms.registerCommand(retAcceleration_C, retAcceleration_T);
    usb_cmd_retRunLevel = m_usbComms.registerCommand(retRunLevel_C, retRunLevel_T);
    usb_cmd_retHomingSpeed = m_usbComms.registerCommand(retHomingSpeed_C, retHomingSpeed_T);
    usb_cmd_retHomingStallValues = m_usbComms.registerCommand(retHomingStallValues_C, retHomingStallValues_T);
    usb_cmd_retHomingTimeout = m_usbComms.registerCommand(retHomingTimeout_C, retHomingTimeout_T);
    usb_cmd_retOverShoot = m_usbComms.registerCommand(retOverShoot_C, retOverShoot_T);
    usb_cmd_retRmsCurrent = m_usbComms.registerCommand(retRmsCurrent_C, retRmsCurrent_T);
    usb_cmd_retFlags = m_usbComms.registerCommand(retFlags_C, retFlags_T);
    usb_cmd_retPowerStatus = m_usbComms.registerCommand(retPowerStatus_C, retPowerStatus_T);
    usb_cmd_retTmcDiag = m_usbComms.registerCommand(retTmcDiag_C, retTmcDiag_T);

    // Settings
    usb_cmd_setPos = m_usbComms.registerCommand(setPos_C, setPos_T, cb_setPos);
    usb_cmd_setNewTarget = m_usbComms.registerCommand(setNewTarget_C, setNewTarget_T, cb_setNewTarget);
    usb_cmd_setMicroSteps = m_usbComms.registerCommand(setMicroSteps_C, setMicroSteps_T, cb_setMicroSteps);
    usb_cmd_setMaxPosition = m_usbComms.registerCommand(setMaxPosition_C, setMaxPosition_T, cb_setMaxPosition);
    usb_cmd_setStepSpeed = m_usbComms.registerCommand(setStepSpeed_C, setStepSpeed_T, cb_setStepSpeed);
    usb_cmd_setAcceleration = m_usbComms.registerCommand(setAcceleration_C, setAcceleration_T, cb_setAcceleration);
    usb_cmd_setHomingSpeed = m_usbComms.registerCommand(setHomingSpeed_C, setHomingSpeed_T, cb_setHomingSpeed);
    usb_cmd_setHomingStallValues = m_usbComms.registerCommand(setHomingStallValues_C, setHomingStallValues_T, cb_setHomingStallValues);
    usb_cmd_setHomingTimeout = m_usbComms.registerCommand(setHomingTimeout_C, setHomingTimeout_T, cb_setHomingTimeout);
    usb_cmd_setOverShoot = m_usbComms.registerCommand(setOverShoot_C, setOverShoot_T, cb_setOverShoot);
    usb_cmd_setRmsCurrent = m_usbComms.registerCommand(setRmsCurrent_C, setRmsCurrent_T, cb_setRmsCurrent);
    usb_cmd_setFlags = m_usbComms.registerCommand(setFlags_C, setFlags_T, cb_setFlags);

    // If the last usb_cmd is 0 we've probably run out of ram while registering commands.
    if(usb_cmd_setFlags == 0)
        error(Error::MEMORY);
}

/*! \brief Run Level = Init routine.

Wait to see if 12v is ok in 3 seconds then initialise the TMC stepper driver. Otherwise enter Run Level = Low volt.
*/
void BullsEyeFocuserActuator::runInit()
{
    const unsigned long now = micros();
    const unsigned long dt = now - m_lastUpdate;

    // Blink leds alternatively every 0.25s
    if(dt > 250000)
    {
        digitalWrite(LEDRED_PIN, !digitalRead(LEDRED_PIN));
        digitalWrite(LEDGRN_PIN, !digitalRead(LEDRED_PIN));
        m_lastUpdate = now;
    }

    // Test 12v after 3s
    if(now > 3000000 && dt > 250000)
    {
        if(powerOk())
        {
            initStepperDriver();
        }
        else
        {
            setRunLevel(RunLevel::RUN_LOWVOLT);
        }
    }
}

/*! \brief Initializes the TMC stepper driver.

Tests the communication between Arduino and the TMC2209 driver. Initializes the driver and sets the Run Level to either Auto Home or Normal depending on the user preference.
*/
void BullsEyeFocuserActuator::initStepperDriver()
{
    resetStepperDriver();
    
    // Test stepper driver comms
    TMC2209Stepper::beginSerial(115200);
    m_driverCommsStatus = TMC2209Stepper::test_connection();
    if(m_driverCommsStatus)
    {
        // Error communicating with stepper driver
        error(Error::DRIVER_COMMS);
    }
    else
    {
        // Stepper driver comms ok. Setup the driver.
        TMC2209Stepper::begin();

        // Sets the slow decay time (off time) [1... 15]. This setting also limits
        // the maximum chopper frequency. For operation with StealthChop,
        // this parameter is not used, but it is required to enable the motor.
        // In case of operation with StealthChop only, any setting is OK.
        TMC2209Stepper::toff(TOFF_VALUE);
        
        // VACTUAL allows moving the motor by UART control.
        // It gives the motor velocity in +-(2^23)-1 [μsteps / t]
        // 0: Normal operation. Driver reacts to STEP input.
        // /=0: Motor moves with the velocity given by VACTUAL. 
        // Step pulses can be monitored via INDEX output.
        // The motor direction is controlled by the sign of VACTUAL.
        TMC2209Stepper::VACTUAL(0);
        
        // Comparator blank time. This time needs to safely cover the switching
        // event and the duration of the ringing on the sense resistor. For most
        // applications, a setting of 16 or 24 is good. For highly capacitive
        // loads, a setting of 32 or 40 will be required.
        TMC2209Stepper::blank_time(24);

        // Disable onboard potentiometer for setting rms current via UART 
        TMC2209Stepper::I_scale_analog(false);
        // Set rms current
        TMC2209Stepper::rms_current(m_rmsCurrent); // mA

        // Select automatic PWM regulation tuning
        TMC2209Stepper::pwm_autoscale(true);
        TMC2209Stepper::pwm_autograd(true);

        // Set default microsteps to m_microSteps
        uint16_t steps = 1 << m_microSteps;
        TMC2209Stepper::microsteps(steps);
        
        // Lower threshold velocity for switching on smart energy CoolStep and StallGuard to DIAG output
        TMC2209Stepper::TCOOLTHRS(0xFFFFF); // 20bit max
        //TMC2209Stepper::TCOOLTHRS(0x00000);
        
        // CoolStep lower threshold [0... 15].
        // If SG_RESULT goes below this threshold, CoolStep increases the current to both coils.
        // 0: disable CoolStep
        TMC2209Stepper::semin(0);
        
        // Sets the number of StallGuard2 readings above the upper threshold necessary
        // for each current decrement of the motor current.
        TMC2209Stepper::sedn(0b01);
        //TMC2209Stepper::sedn(0b10);
        
        // StallGuard4 threshold [0... 255] level for stall detection. It compensates for
        // motor specific characteristics and controls sensitivity. A higher value gives a higher
        // sensitivity. A higher value makes StallGuard4 more sensitive and requires less torque to
        // indicate a stall. The double of this value is compared to SG_RESULT.
        // The stall output becomes active if SG_RESULT fall below this value.
        TMC2209Stepper::SGTHRS(m_homingStallOutValue);

        // INDEX output shows step pulses from internal pulse generator (toggle upon each step)
        TMC2209Stepper::index_step(true);

        // Invert motor direction if flag is set
        TMC2209Stepper::shaft(m_flags & FLAG_INVERTDIR);

        AccelStepper::enableOutputs();
        delay(100);

        // Go to auto home.
        digitalWrite(LEDRED_PIN, HIGH);
        digitalWrite(LEDGRN_PIN, HIGH);

        if(m_flags & FLAG_USEAUTOHOME)
            setRunLevel(RunLevel::RUN_AUTOHOME);
        else
            setRunLevel(RunLevel::RUN_NORMAL);
    }
}

/*! \brief Additional TMC2209 tuning after Auto Home. */
void BullsEyeFocuserActuator::tuneStepperDriver()
{
    // CoolStep lower threshold [0... 15].
    // If SG_RESULT goes below this threshold, CoolStep increases the current to both coils.
    // 0: disable CoolStep
    TMC2209Stepper::semin(15);
    // TMC2209Stepper::semin(0);

    // CoolStep upper threshold [0... 15].
    // If SG is sampled equal to or above this threshold enough times,
    // CoolStep decreases the current to both coils.
    //TMC2209Stepper::semax(2);
    TMC2209Stepper::semax(15);

    TMC2209Stepper::SGTHRS(constrain(min(m_homingStallInValue, m_homingStallOutValue) - 5, 0, 255));
}

/*! \brief Resets the TMC2209. */
void BullsEyeFocuserActuator::resetStepperDriver()
{
    delay(100);
    digitalWrite(EN_PIN, LOW);
    digitalWrite(DIR_PIN, LOW);
    digitalWrite(STEP_PIN, LOW);
    digitalWrite(VIO_PIN, LOW);
    delay(100);
    digitalWrite(VIO_PIN, HIGH);
    digitalWrite(EN_PIN, HIGH);
}

/*! \brief Run Level = Low Voltage routine.

Wait to see if 12v is ok then re-initialize TMC2209 driver and set Run Level to either Auto Home or Normal depending on the user preference.
*/
void BullsEyeFocuserActuator::runLowVolt()
{
    if(powerOk())
    {
        digitalWrite(LEDRED_PIN, HIGH);
        digitalWrite(LEDGRN_PIN, HIGH);
        initStepperDriver();
        TMC2209Stepper::VACTUAL(0);
        AccelStepper::enableOutputs();
        if(m_flags & FLAG_USEAUTOHOME)
        {
            m_ahStatus = AutoHome::AH_INIT;
            setRunLevel(RunLevel::RUN_AUTOHOME);
        }
        else
        {
            setRunLevel(RunLevel::RUN_NORMAL);
        }
        delay(1000);
        return;
    }

    const unsigned long now = micros();
    const unsigned long dt = now - m_lastUpdate;

    if(dt > 1000000)
    {
        digitalWrite(LEDRED_PIN, !digitalRead(LEDRED_PIN));
        digitalWrite(LEDGRN_PIN, !digitalRead(LEDRED_PIN));
        m_lastUpdate = now;
    }
}

/*! \brief Run Level = Auto Home routine.

While power is ok, runs the auto homing routine. First it will rack the focuser out as far as it can. Then back in again as far as it can while
counting the microsteps. Then out again by homing marigin. Time out error will be triggered if the procedure does not complete in the user specified time.
If 12v is lost during this procedure, Run Level will be switched back to Low Voltage.
*/
void BullsEyeFocuserActuator::runAutoHome()
{
    if(!powerOk())
    {
        AccelStepper::disableOutputs();
        m_ahStatus = AutoHome::AH_INIT;
        setRunLevel(RunLevel::RUN_LOWVOLT);
        return;
    }

    const unsigned long now = micros();
    unsigned long dt = now - m_lastUpdate;

    digitalWrite(LEDRED_PIN, HIGH);
    if(dt > 250000)
    {
        digitalWrite(LEDGRN_PIN, !digitalRead(LEDGRN_PIN));
        m_lastUpdate = now;
    }


    const uint32_t ignoreSteps = ((1 << (uint32_t)m_microSteps) * (uint32_t)HOMING_IGNORE);
    switch(m_ahStatus)
    {
    case AutoHome::AH_INIT:
        m_timeoutCount = micros();
        s_idxCount = 0;
        s_diagEnable = false;
        m_ahStatus = AutoHome::AH_RUN_OUT;
        break;

    case AutoHome::AH_RUN_OUT:
        if(TMC2209Stepper::VACTUAL() == 0)
        {
            TMC2209Stepper::VACTUAL(-m_homingSpeed);
        }
        else if(s_idxCount > ignoreSteps)
        {
            s_diagEnable = true;
        }
        break;

    case AutoHome::AH_RUN_IN:
        if(TMC2209Stepper::VACTUAL() == 0)
        {
            TMC2209Stepper::VACTUAL(m_homingSpeed);
        }
        else if(s_idxCount > ignoreSteps)
        {
            s_diagEnable = true;
        }
        break;

    case AutoHome::AH_RUN_HOME:
        if(m_flags & FLAG_USEACCEL)
            AccelStepper::run();
        else
            AccelStepper::runSpeedToPosition();
        if(AccelStepper::distanceToGo() == 0)
        {
            AccelStepper::setCurrentPosition(0);
            usb_cmd_retCurPosition->send(0);
            m_ahStatus = AutoHome::AH_COMPLETED;
        }
        break;

    case AutoHome::AH_COMPLETED:
        digitalWrite(LEDRED_PIN, HIGH);
        digitalWrite(LEDGRN_PIN, HIGH);
        s_diagEnable = true;
        setRunLevel(RunLevel::RUN_NORMAL);
        return;
        
    default:
        error(Error::UNKNOWN);
    }

    dt = micros() - m_timeoutCount;
    if(dt > ((uint32_t)m_homingTimeout * 1000000))
    {
        // Time out error. Auto home didn't complete in time.
        error(Error::AH_TIMEOUT);
    }
}

/*! \brief Run Level = Normal routine.

While 12v power is ok, run normally. Use over shoot backlash compensation in the case of focusing outward.
*/
void BullsEyeFocuserActuator::runNormal()
{
    if(!powerOk())
    {
        AccelStepper::disableOutputs();
        setRunLevel(RunLevel::RUN_LOWVOLT);
        return;
    }

    if(m_flags & FLAG_USEACCEL)
        AccelStepper::run();
    else
        AccelStepper::runSpeedToPosition();

    if(m_curOverShoot > 0 && AccelStepper::distanceToGo() == 0)
    {
        delay(500);
        m_curTarget -= m_curOverShoot;
        m_curOverShoot = 0;
        setNewTarget(m_curTarget);
        goToTarget();
        usb_cmd_retCurTarget->send(m_curTarget);
    }
}

/*! \brief Run Level = Error routine.

Stop the motor and report the error code every 5 seconds.
*/
void BullsEyeFocuserActuator::runError()
{
    if(TMC2209Stepper::VACTUAL() != 0)
        TMC2209Stepper::VACTUAL(0);

    if(digitalRead(EN_PIN) == LOW)
    {
        AccelStepper::setSpeed(0);
        AccelStepper::stop();
        AccelStepper::disableOutputs();
    }

    unsigned long now = micros();
    unsigned long dt = now - m_lastUpdate;

    digitalWrite(LEDGRN_PIN, HIGH);
    if(dt > 125000)
    {
        digitalWrite(LEDRED_PIN, !digitalRead(LEDRED_PIN));
        m_lastUpdate = now;
    }

    dt = now - m_timeoutCount;
    if(dt > 5000000)
    {
        usb_cmd_error->send(m_error);
        m_timeoutCount = now;
    }
}

/*! \brief The main update routine.

This routine is called periodically from the main Arduino loop.
*/
void BullsEyeFocuserActuator::update()
{
    switch (m_runLevel)
    {
    case RunLevel::RUN_INIT:
        runInit();
        break;
    case RunLevel::RUN_LOWVOLT:
        runLowVolt();
        break;
    case RunLevel::RUN_AUTOHOME:
        runAutoHome();
        break;
    case RunLevel::RUN_NORMAL:
        runNormal();
        break;
    case RunLevel::RUN_ERROR:
        runError();
        break;
    default:
        error(Error::UNKNOWN);
        break;
    }
}

/*! \brief Return true if 12v power is ok. Otherwise false. */
bool BullsEyeFocuserActuator::powerOk()
{
    m_pwrRaw = analogRead(PWR_MON_PIN);
    return m_pwrRaw > 325;
}

/*! \brief Resets the Arduino. */
void BullsEyeFocuserActuator::reset()
{
    resetFunc();
}

/*! \brief Sets the Run Level = Error.

@param param The error code.
*/
void BullsEyeFocuserActuator::error(const uint16_t &param)
{
    setRunLevel(RunLevel::RUN_ERROR);
    m_error = param;
    usb_cmd_error->send(m_error);
}

/*! \brief Return the current Run Level. */
uint8_t BullsEyeFocuserActuator::getRunLevel()
{
    return m_runLevel;
}

/*! \brief Sets the Run Level.

Stops the motor if Run Level is Low Volt or Error. Calls tuneStepperDriver if Run Level = Normal.

@param runLevel The Run Level to be entered.
*/
void BullsEyeFocuserActuator::setRunLevel(const uint8_t &runLevel)
{
    if(runLevel == RunLevel::RUN_LOWVOLT || runLevel == RunLevel::RUN_ERROR)
    {
        AccelStepper::setSpeed(0);
        AccelStepper::stop();
    }

    usb_cmd_retRunLevel->send(runLevel);
    m_runLevel = runLevel;

    if(m_runLevel == RunLevel::RUN_NORMAL)
        tuneStepperDriver();
}

/*! \brief Return the Device Id.

@return The Device Id. 16 MSB = Device family, 16 LSB = Device Id.
*/
uint32_t BullsEyeFocuserActuator::getDeviceId()
{
    return DEVICE_ID;
}

/*! \brief Return the RMS Current.

@return RMS Current in mA.
*/
uint16_t BullsEyeFocuserActuator::getRmsCurrent()
{
    return m_rmsCurrent;
}

/*! \brief Return the current stepper motor position.

@return Motor position.
*/
uint32_t BullsEyeFocuserActuator::getCurPosition()
{
    return AccelStepper::currentPosition();
}

/*! \brief Return the current stepper motor target.

@return Current motor target position.
*/
uint32_t BullsEyeFocuserActuator::getCurTarget()
{
    return m_curTarget;
}

/*! \brief Return the current stepper motor status.

@return 0 = Stopped.
@return -1 = Running inwards.
@return 1 = Running outwards.
 */
int8_t BullsEyeFocuserActuator::getMotorStatus()
{
    if(AccelStepper::distanceToGo() == 0 || !powerOk())
        return 0;

    if(getCurPosition() < m_curTarget)
        return 1;
    else
        return -1;
}

/*! \brief Return the current micro steps setting.

@return Micro Steps = 2 ^ return value.
*/
uint8_t BullsEyeFocuserActuator::getMicroSteps()
{
    return m_microSteps;
}

/*! \brief Return the maximum position.

@return Maxumum position.
*/
uint32_t BullsEyeFocuserActuator::getMaxPosition()
{
    return m_maxPosition;
}

/*! \brief Return the current stepping speed.

@return Stepping speed in microsteps per second.
*/
uint16_t BullsEyeFocuserActuator::getStepSpeed()
{
    return m_maxStepSpeed;
}

/*! \brief Return the current temperature.

@return Temperature in celcius. Only takes a new reading if the motor is not running.
*/
float BullsEyeFocuserActuator::getTemperature()
{
    // This function is slow (~500ms). Return previous temperature if focuser if moving.
    if(AccelStepper::distanceToGo() != 0)
        return m_temperature;

    m_dallasTemperature->requestTemperatures();
    m_temperature = m_dallasTemperature->getTempCByIndex(0);

    return m_temperature;
}

/*! \brief Return the driver status.

@return 1 if enabled, otherwise 0.
*/
uint8_t BullsEyeFocuserActuator::getDriverStatus()
{
    uint8_t s = digitalRead(EN_PIN);
    return 1 - s;
}

/*! \brief Return the current acceleration.

@return Current acceleration in microsteps per second per second.
*/
uint16_t BullsEyeFocuserActuator::getAcceleration()
{
    return m_acceleration;
}

/*! \brief Return the homing speed.

@return Speed at which the auto homing is done with in microsteps per second.
*/
uint16_t BullsEyeFocuserActuator::getHomingSpeed()
{
    return m_homingSpeed;
}

/*! \brief Return the auto homing stall values.

@return The inward and outward motion stall values packed into a single 16 value. 8 MSB = In stall value, 8 LSB = Out stall value.
*/
uint16_t BullsEyeFocuserActuator::getHomingStallValues()
{
    uint16_t homingStallValues = m_homingStallInValue << 8 | m_homingStallOutValue;
    return homingStallValues;
}

/*! \brief Return the homing timeout value.

@return Auto homing timeout in seconds.
*/
uint8_t BullsEyeFocuserActuator::getHomingTimeout()
{
    return m_homingTimeout;
}

/*! \brief Return the overshoot value.

@return Overshoot in microsteps.
*/
uint16_t BullsEyeFocuserActuator::getOverShoot()
{
    return m_overShoot;
}

/*! \brief Return the flags.

@return The flags as a 16 bit value. Bit 0 = Use auto home. Bit 1 = Invert motor direction. Bit 2 = Use acceleration. Bits 3 - 16 reserved.
*/
uint8_t BullsEyeFocuserActuator::getFlags()
{
    return m_flags;
}

/*! \brief Return the Vcc voltage.

Measures the Vcc voltage against the internal 1.1V reference.

@return Vcc in millivolts.
*/
uint32_t BullsEyeFocuserActuator::readVcc()
{
    uint32_t result;
    // Read 1.1V reference against AVcc
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
    delay(2); // Wait for Vref to settle
    ADCSRA |= _BV(ADSC); // Convert
    while (bit_is_set(ADCSRA,ADSC));
    result = ADCL;
    result |= ADCH<<8;
    result = 1126400L / result; // Back-calculate AVcc in mV
    return result;
}

/*! \brief Return the power status.

Reads and calculates the voltage at the 12v input. Returns Vcc instead if 12v is not connected.
Accuracy of this depends on the accuracy of measured R3 and R4 resistances and the accuracy of the internal 1.1V reference voltage.

@return Input voltage or Vcc in millivolts.
*/
uint32_t BullsEyeFocuserActuator::getPowerStatus()
{
    uint32_t Vcc = readVcc();                   // mV

    const uint32_t r3 = R3;                     // ohm
    const uint32_t r4 = R4;                     // ohm

    const uint32_t Vinmin = 10000000;           // uV
    const uint32_t Vinmax = 15000000;           // uV


    const uint32_t Imin = Vinmin / (r3 + r4);   // uA
    const uint32_t Vmin = (Imin * r4) / 1000;   // mV
    
    const uint32_t Imax = Vinmax / (r3 + r4);   // uA
    const uint32_t Vmax = (Imax * r4) / 1000;   // mV

    const uint32_t Vr4 = map(m_pwrRaw, 0, 1023, 0, Vcc);

    const uint32_t Vpwr = map(Vr4, Vmin, Vmax, Vinmin / 1000, Vinmax / 1000); // mV

    if(Vpwr < Vcc)
        return Vcc;     // mV
    else
        return Vpwr;    // mV
}

/*! \brief Sets the stepper motor RMS current.

@param param RMS current in milliamperes.
*/
void BullsEyeFocuserActuator::setRmsCurrent(const uint16_t &param)
{
    if(param <= 1770 && m_runLevel != RunLevel::RUN_AUTOHOME && getMotorStatus() == 0)
    {
        m_rmsCurrent = param;
        if(powerOk())
            TMC2209Stepper::rms_current(m_rmsCurrent);
        if(m_runLevel > RunLevel::RUN_INIT)
            saveSettings();
    }
    usb_cmd_retRmsCurrent->send(m_rmsCurrent);
}

/*! \brief Sets the current position.

This will set the current position without moving the motor.

@param param The desired position.
*/
void BullsEyeFocuserActuator::setPosition(const uint32_t &param)
{
    if(param <= m_maxPosition && m_runLevel != RunLevel::RUN_AUTOHOME && getMotorStatus() == 0)
    {
        AccelStepper::setCurrentPosition((long)param);
    }
    usb_cmd_retCurPosition->send(AccelStepper::currentPosition());
}

/*! \brief Sets the micro stepping value.

The desired microstepping is a function of power of two where the param is the exponent. Valid param values are 0-8. 
For example param = 6 will yield to 64 micro steps.

@param param The exponent of power of two.
*/
void BullsEyeFocuserActuator::setMicroSteps(const uint8_t &param)
{
    if(param >= 0 && param <= 8 && m_runLevel != RunLevel::RUN_AUTOHOME && getMotorStatus() == 0)
    {
        m_microSteps = param;
        uint16_t steps = 1 << m_microSteps;
        if(powerOk())
            TMC2209Stepper::microsteps(steps);
        if(m_runLevel > RunLevel::RUN_INIT)
            saveSettings();
    }
    usb_cmd_retMicroSteps->send(m_microSteps);
}

/*! \brief Sets the maximum focuser position.

@param param The maximum position in micro steps.
*/
void BullsEyeFocuserActuator::setMaxPosition(const uint32_t &param)
{
    if(param <= 0x7FFFFFFF && getMotorStatus() == 0)
    {
        m_maxPosition = param;
        if(m_runLevel > RunLevel::RUN_INIT)
            saveSettings();
    }
    usb_cmd_retMaxPosition->send(m_maxPosition);
}

/*! \brief Sets the stepping speed.

This is the maximum speed the motor will run at.

@param param The speed in microsteps per second.
*/
void BullsEyeFocuserActuator::setStepSpeed(const uint16_t &param)
{
    if(param <= MAX_SPEED && m_runLevel != RunLevel::RUN_AUTOHOME && getMotorStatus() == 0)
    {
        m_maxStepSpeed = param;
        AccelStepper::setMaxSpeed(m_maxStepSpeed);
        if(m_runLevel > RunLevel::RUN_INIT)
            saveSettings();
    }
    usb_cmd_retStepSpeed->send(m_maxStepSpeed);
}

/*! \brief Sets the acceleration value.

@param param Acceleration in microsteps per second per second.
*/
void BullsEyeFocuserActuator::setAcceleration(const uint16_t &param)
{
    if(m_runLevel != RunLevel::RUN_AUTOHOME && getMotorStatus() == 0)
    {
        m_acceleration = param;
        AccelStepper::setAcceleration(m_acceleration);
        if(m_runLevel > RunLevel::RUN_INIT)
            saveSettings();
    }
    usb_cmd_retAcceleration->send(m_acceleration);
}

/*! \brief Sets the auto homing speed.

@param param Speed in microsteps per second.
*/
void BullsEyeFocuserActuator::setHomingSpeed(const int16_t &param)
{
    if(m_runLevel != RunLevel::RUN_AUTOHOME && getMotorStatus() == 0)
    {
        m_homingSpeed = param;
        if(m_runLevel > RunLevel::RUN_INIT)
            saveSettings();
    }
    usb_cmd_retHomingSpeed->send(m_homingSpeed);
}

/*! \brief Sets the auto homing stall values.

@param param The inward and outward motion stall values packed into a single 16 value. 8 MSB = In stall value, 8 LSB = Out stall value.
*/
void BullsEyeFocuserActuator::setHomingStallValues(const uint16_t &param)
{
    if(m_runLevel != RunLevel::RUN_AUTOHOME && getMotorStatus() == 0)
    {
        m_homingStallOutValue = param & 0xFF;
        m_homingStallInValue = (param >> 8) & 0xFF;
        if(powerOk())
            TMC2209Stepper::SGTHRS(m_homingStallOutValue);
        if(m_runLevel > RunLevel::RUN_INIT)
            saveSettings();
    }

    uint16_t homingStallValues = getHomingStallValues();
    usb_cmd_retHomingStallValues->send(homingStallValues);
}

/*! \brief Sets the auto homing timeout value.

@param param The timeout in seconds.
*/
void BullsEyeFocuserActuator::setHomingTimeout(const uint8_t &param)
{
    m_homingTimeout = param;
    if(m_runLevel > RunLevel::RUN_INIT)
        saveSettings();

    usb_cmd_retHomingTimeout->send(m_homingTimeout);
}

/*! \brief Set the overshoot value.

@param param Overshoot in microsteps.
*/
void BullsEyeFocuserActuator::setOverShoot(const uint16_t &param)
{
    if(m_runLevel != RunLevel::RUN_AUTOHOME && getMotorStatus() == 0)
    {
        m_overShoot = param;
        if(m_flags & FLAG_USEAUTOHOME)
            setMaxPosition(m_absoluteMaxPosition - 2 * m_homingMarigin - m_overShoot);
        if(m_runLevel > RunLevel::RUN_INIT)
            saveSettings();
    }
    usb_cmd_retOverShoot->send(m_overShoot);
}

/*! \brief Set the flags.

@param param The flags as a 16 bit value. Bit 0 = Use auto home. Bit 1 = Invert motor direction. Bit 2 = Use acceleration. Bits 3 - 16 reserved.
*/
void BullsEyeFocuserActuator::setFlags(const uint8_t &param)
{
    if(m_runLevel != RunLevel::RUN_AUTOHOME && getMotorStatus() == 0)
    {
        m_flags = param;
        if(m_runLevel > RunLevel::RUN_INIT)
            saveSettings();
    }

    if(powerOk())
        TMC2209Stepper::shaft(m_flags & FLAG_INVERTDIR);
    AccelStepper::stop();
    AccelStepper::setSpeed(0.0);
    m_curTarget = getCurPosition();

    usb_cmd_retFlags->send(m_flags);
}

/*! \brief Stops the motor. */
void BullsEyeFocuserActuator::stop()
{
    if(m_runLevel == RunLevel::RUN_NORMAL)
    {
        AccelStepper::stop();
        AccelStepper::setSpeed(0.0);
        m_curTarget = AccelStepper::targetPosition();
    }
}

/*! \brief Moves the focuser to the current target position. */
void BullsEyeFocuserActuator::goToTarget()
{
    if(m_runLevel == RunLevel::RUN_NORMAL)
    {
        AccelStepper::moveTo(m_curTarget);
        if(!(m_flags & FLAG_USEACCEL))
            AccelStepper::setSpeed(m_maxStepSpeed);
        usb_cmd_retCurTarget->send(m_curTarget);
        usb_cmd_retMotorStatus->send(getMotorStatus());
    }
}

/*! \brief Enables or disables power to the stepper motor.

@param param 0 = Disable motor. 1 = Enable motor.
*/
void BullsEyeFocuserActuator::engage(const uint8_t &param)
{
    if(param == 0 && m_runLevel == RunLevel::RUN_NORMAL)
    {
        AccelStepper::disableOutputs();
    }
    else
    {
        AccelStepper::enableOutputs();
    }
}

/*! \brief Sets new target position.

@param param Position in microsteps.
*/
void BullsEyeFocuserActuator::setNewTarget(const uint32_t &param)
{
    if(param <= m_maxPosition && m_runLevel == RunLevel::RUN_NORMAL)
    {
        if(param > getCurPosition())
            m_curOverShoot = m_overShoot;
        else
            m_curOverShoot = 0;

        m_curTarget = param + m_curOverShoot;
    }

    usb_cmd_retCurTarget->send(m_curTarget);
}

/*! \brief Sends the limit status to the INDI driver. */
void BullsEyeFocuserActuator::retLimitStatus()
{
        uint32_t pos = getCurPosition();
        if(pos == 0)
        {
            m_minLimit = 1;
            m_maxLimit = 0;
        }
        else if(pos >= m_maxPosition)
        {
            m_minLimit = 0;
            m_maxLimit = 1;
        }
        else
        {
            m_minLimit = 0;
            m_maxLimit = 0;
        }

        if(m_minLimit != m_lastMinLimit)
            usb_cmd_minLimit->send(m_minLimit);

        if(m_maxLimit != m_lastMaxLimit)
            usb_cmd_maxLimit->send(m_maxLimit);

        m_lastMinLimit = m_minLimit;
        m_lastMaxLimit = m_maxLimit;
}
