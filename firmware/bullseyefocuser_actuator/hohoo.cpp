/*******************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

#include "hohoo.hpp"

/*!
\brief Constructor
*/
HoHoo::HoHoo() :
    m_send_callback(0),
    m_checksum(0),
    m_current_packet_pos(0),
    m_num_commands(0)
{
    m_commands = NULL;
}

/*!
\brief Destructor

Deletes the registered commands and frees the memory allocated for them.
*/
HoHoo::~HoHoo()
{
    if(m_commands)
    {
        for(uint16_t i = 0; i < m_num_commands; i++)
            delete m_commands[i];
        free(m_commands);
    }
}

/*!
\brief Sets the callback to be used for sending data.

@param send_callback Pointer to the send callback function.
*/
void HoHoo::setSendCallback(void(*send_callback)(const uint8_t &count, const uint8_t *buf))
{
    m_send_callback = send_callback;
}

/*!
\brief Updates the internal buffer with data.

Called by HoHoo::update()

@param c the byte to update the buffer with.

\return true on success.
\return false on failure.
*/
bool HoHoo::updateData(const uint8_t &c)
{
    if(m_current_packet_pos > MAX_BUF_LEN)
        return false;

    m_buffer[m_current_packet_pos] = c;
    m_current_packet_pos++;
    m_checksum += c;

    return true;
}


/*!
\brief Discards the current internal buffer.

Resets the internal packet position and check sum to 0.
*/
void HoHoo::discardBuffer(const CommErr &e)
{
    m_current_packet_pos = 0;
    m_checksum = 0;
}

/*!
\brief Interprets the incoming byte stream one byte at a time.

This function is to be called with every received byte. As long as the pattern
of the received data matches the communication protocol and the check sum matches,
a user implemented run function of a registered command will be called.

Upon error in the stream, the buffer received so far will be discarded.

@param c The received data.

\sa HoHoo::registerCommand()
*/
void HoHoo::update(const uint8_t &c)
{
    // Deal with header data first
    if(m_current_packet_pos < 4)
    {
        switch(m_current_packet_pos)
        {
        case 0: // SB
            if(c == SB)
            {
                if(!updateData(c))
                {
                    discardBuffer(COMM_ERR_BUF);
                    return;
                }
            }
            else
                discardBuffer(COMM_ERR_SB);
            return;
        case 1: // ML
            if(c >= ML_MIN && c <= ML_MAX)
            {
                if(!updateData(c))
                {
                    discardBuffer(COMM_ERR_BUF);
                    return;
                }
                m_data_length = c - ML_MIN;
            }
            else
            {
                discardBuffer(COMM_ERR_ML);
            }
            return;
        case 2: // CMD
            // Actual command will be checked if checksum is ok
            if(!updateData(c))
            {
                discardBuffer(COMM_ERR_BUF);
                return;
            }
            return;
        case 3: // DT
            if(c <= 0x07)
            {
                if( (c == 0x00 && m_data_length != 0) ||
                    (c >= 0x01 && c <= 0x02 && m_data_length != 1) ||
                    (c >= 0x03 && c <= 0x04 && m_data_length != 2) ||
                    (c >= 0x05 && c <= 0x07 && m_data_length != 4) )
                    discardBuffer(COMM_ERR_ML);
                else
                {
                    if(!updateData(c))
                    {
                        discardBuffer(COMM_ERR_BUF);
                        return;
                    }
                }
            }
            else
                discardBuffer(COMM_ERR_DT);
            return;
        }
    }

    // Deal with data next 0, 1, 2 or 4 bytes.
    if(m_current_packet_pos - HDR_LEN < m_data_length)
    {
        if(!updateData(c))
        {
            discardBuffer(COMM_ERR_DATA);
            return;
        }
        return;
    }

    // End byte
    if(m_current_packet_pos == m_data_length + HDR_LEN)
    {
        if(c == EB)
        {
            if(!updateData(c))
            {
                discardBuffer(COMM_ERR_BUF);
                return;
            }
            return;
        }
        else
        {
            discardBuffer(COMM_ERR_EB);
            return;
        }
    }

    // Checksum
    if(c == m_checksum)
    {
        if(!updateData(c))
        {
            discardBuffer(COMM_ERR_BUF);
            return;
        }
        m_current_packet_pos = 0;
        m_checksum = 0;

        for(uint16_t i = 0; i < m_num_commands; i++)
        {
            if(*m_commands[i] == m_buffer[2])
            {
                if(m_commands[i]->getParamType() == m_buffer[3])
                    m_commands[i]->run(&m_buffer[4]);

                return;
            }
        }
    }
    else
    {
        discardBuffer(COMM_ERR_CS);
    }
}

/*!
\brief Instantiates and stores a new command in the list of known commands.

Called by HoHoo::registerCommand()

@param cmd The command number. Must be unique.

\return Pointer to a new instance of class Command or 0 on failure.
*/
Command* HoHoo::newCommand(const uint8_t &cmd)
{

    for(uint16_t i = 0; i < m_num_commands; i++)
        if(*m_commands[i] == cmd)
        {
            return 0;
        }

    Command* *commands;
    commands = (Command**)realloc(m_commands, ++m_num_commands * sizeof(Command*));

    if(commands)
    {
        m_commands = commands;
        const uint16_t idx = m_num_commands - 1;
        m_commands[idx] = new Command(this);
        return m_commands[idx];       
    }
    else
    {
        return 0;
    }
}

/*!
\brief Registers a new command.

@param cmd The command number. Must be unique.
@param param_type Data type of the parameter the command accepts.

\return Pointer to a new instance of class Command or 0 on failure.

\sa HoHoo::newCommand()
*/
Command* HoHoo::registerCommand(const uint8_t &cmd, const uint8_t &param_type)
{
    Command *command = newCommand(cmd);

    if(!command)
        return 0;

    if(command->registerCommand(cmd, param_type))
        return command;

    return 0;
}
