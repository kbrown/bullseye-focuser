/*******************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef HOHOO_H
#define HOHOO_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "command.hpp"
#include "callbackbase.hpp"

/*!
\brief Maximum buffer length
*/
#define MAX_BUF_LEN 10

/*!
\page protocol Communication Protocol
\verbatim
Message format:     SB, ML, CMD, DT, DATA, EB, CS

BYTE        DESCRIPTION         SIZE            VALUE / RANGE
------------------------------------------------------------------------------
SB          Start byte          1 byte          0xD2
ML          Message length      1 byte          0x06 - 0x0A / x
CMD         Command             1 byte          0x00 - 0xFE
DT          Data type           1 byte          0x00 - 0x08
DATA        Data                0-4 bytes / x   0x00 - 0xFFFF / x
EB          End byte            1 byte          0xF0
CS          Check sum           1 byte          0x00 - 0xFF

Data TYPES (DT):
BYTE        DESCRIPTION                         SIZE
------------------------------------------------------------------------------
0x00        no data                             0 bytes
0x01        int8_t                              1 byte
0x02        uint8_t                             1 byte
0x03        int16_t                             2 bytes
0x04        uint16_t                            2 bytes
0x05        int32_t                             4 bytes
0x06        uint32_t                            4 bytes
0x07        float                               4 bytes
0x08        blob                                x bytes
\endverbatim
*/

/*!
@file
\brief HoHoo header file
*/

/*!
\brief Enumeration of command data types
*/
enum DataTypes : uint8_t
{
    VOID_T      = 0x00,
    INT8_T      = 0x01,
    UINT8_T     = 0x02,
    INT16_T     = 0x03,
    UINT16_T    = 0x04,
    INT32_T     = 0x05,
    UINT32_T    = 0x06,
    FLOAT_T     = 0x07,
    BLOB_T      = 0x08
};

/*!
\brief Enumeration of communication error codes.
*/
enum CommErr : uint8_t
{
    COMM_ERR_SB     = 0x00,
    COMM_ERR_ML     = 0x01,
    COMM_ERR_CMD    = 0x02,
    COMM_ERR_DT     = 0x03,
    COMM_ERR_DATA   = 0x04,
    COMM_ERR_EB     = 0x05,
    COMM_ERR_CS     = 0x06,
    COMM_ERR_BUF    = 0x07
};

/*! \brief Main class for HoHoo communication protocl. */
class HoHoo
{
public:
    HoHoo();
    virtual ~HoHoo();

    void        setSendCallback(void(*m_send_callback)(const uint8_t &count, const uint8_t *buf));

    void        update(const uint8_t &c);

    template <typename T>
    Command*    registerCommand(const uint8_t &cmd, const uint8_t &param_type, CallbackBase<T> *callback);
    Command*    registerCommand(const uint8_t &cmd, const uint8_t &param_type);

    /*! \brief Pointer to the send callback function. */
    void        (*m_send_callback)(const uint8_t &count, const uint8_t *buf);
    /*! \brief The internal byte stream buffer. */
    uint8_t     m_buffer[MAX_BUF_LEN];

private:
    Command*    newCommand(const uint8_t &cmd);
    bool        updateData(const uint8_t &c);
    void        discardBuffer(const CommErr &e);

    /*! \brief Stores the checksum of a command. */
    uint8_t     m_checksum;
    /*! \brief The position of the current packet in the buffer. */
    uint8_t     m_current_packet_pos;
    /*! \brief Length of the data section of a command. */
    uint8_t     m_data_length;

    /*! \brief Array of registered Command pointers. */
    Command*   *m_commands;
    /*! \brief Number of registered commands. */
    uint16_t    m_num_commands;
};

/*!
\brief Registers a new command with an associated callback.

@param cmd The command number. Must be unique.
@param param_type Data type of the parameter the command accepts.
@param callback Instace of class CallbackBase

\return Pointer to a new instance of class Command or 0 on failure.

\sa HoHoo::newCommand()
*/
template <typename T>
Command* HoHoo::registerCommand(const uint8_t &cmd, const uint8_t &param_type, CallbackBase<T> *callback)
{
    Command *command = newCommand(cmd);

    if(!command)
        return 0;

    if(command->registerCommand(cmd, param_type, callback))
    {
        callback->setHohoo(this);
        return command;
    }

    return 0;
}

#endif