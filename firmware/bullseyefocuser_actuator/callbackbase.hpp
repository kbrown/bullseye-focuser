/*****************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef CALLBACKBASE_H
#define CALLBACKBASE_H

#include <stdint.h>

/*! \brief Common functionality for all CallbackBase overloads */
class CallbackCommon
{
public:
    CallbackCommon() : m_hohoo(0)
    {

    }

    /*!
    Stores the pointer to an instance of class HoHoo

    \todo Uses a void pointer. Really should check for the type.
    */
    void setHohoo(void *hohoo)
    {
        m_hohoo = hohoo;
    }

private:
    /*! \brief Pointer to an instance of class HoHoo. */
    void *m_hohoo;
};

/*! \brief CallbackBase version with a parameter. */
template <typename T>
class CallbackBase : public CallbackCommon
{
public:
    CallbackBase()
    {
    }

    /*!
    Pure virtual function. Will be called by HoHoo::update(). Instances of CallbackBase must implement this function.
    */
    virtual void run(const T &param) = 0;
};

/*! \brief CallbackBase version without a parameter. */
template <>
class CallbackBase<void> : public CallbackCommon
{
public:
    CallbackBase()
    {
    }

    /*!
    Pure virtual function. Will be called by HoHoo::update(). Instances of CallbackBase must implement this function.
    */
    virtual void run() = 0;
};

#endif