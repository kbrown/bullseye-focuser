/*****************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*****************************************************************************/

#include <stdint.h>

#include "bullseyefocuser_actuator.hpp"

/*! \brief Static pointer to store an instance of BullsEyeFocuserActuator class. */
static BullsEyeFocuserActuator *bullsEyeFocuserActuator;

/*!
@file
\brief The main Arduino project file.
*/

/*!
\brief Serial comms send callback.

This will be registered with HoHoo for serial communications sends.

@param count Amount of bytes to send.
@param buf The buffer containing the data to be sent.
*/
void sendToUSB(const uint8_t &count, const uint8_t *buf)
{
    for(uint8_t i = 0; i < count; i++)
    {
        Serial.write(buf[i]);
    }
}

/*!
\brief Initialize serial communications and BullsEyeFocuserActuator class.

Run once upon power on.
*/
void setup()
{
    Serial.begin(9600);

    bullsEyeFocuserActuator = new BullsEyeFocuserActuator(sendToUSB);
}

/*!
\brief Main loop.

Updates BullsEyeFocuser class and sends it any available data received from serial comms.
*/
void loop()
{
    bullsEyeFocuserActuator->update();

    int bytes_avail = Serial.available();
    if(bytes_avail > 0)
    {
        for(int i = 0; i < bytes_avail; i++)
        {
            uint8_t byte = Serial.read();
            bullsEyeFocuserActuator->m_usbComms.update(byte);
        }
    }
}
