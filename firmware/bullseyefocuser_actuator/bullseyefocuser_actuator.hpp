/*****************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef BULLSEYEFOCUSER_ACTUATOR_H
#define BULLSEYEFOCUSER_ACTUATOR_H

#include <stdint.h>

#include <Arduino.h>
#include <EEPROMex.h>
#include <TMCStepper.h>
#include <AccelStepper.h>
#include <OneWire.h> 
#include <DallasTemperature.h>

#include "hohoo.hpp"
#include "bullseyefocuser_commands.hpp"
#include "bullseyefocuser_actuator_callbacks.hpp"

#define R3  46500   // ohms
#define R4   9920   // ohms

#define FASTADC 1
// defines for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

#define VERSION             (uint8_t)101
#define DEVICE_ID           (uint32_t)0x00010002

#define RMS_CURRENT         (uint16_t)400
#define MICROSTEPS          (uint8_t)6
#define MAX_SPEED           (uint16_t)10000
#define STEP_SPEED          (uint16_t)4000
#define ACCELERATION        (uint16_t)1000
#define HOMING_SPEED        (uint16_t)STEP_SPEED
#define HOMING_IGNORE       (uint8_t)5
#define HOMING_TIMEOUT      (uint8_t)45
#define OVERSHOOT           (uint16_t)100

#define DRIVER_ADDRESS      (uint8_t)0b00   // TMC2209 Driver address according to MS1 and MS2
#define STALL_VALUE         (uint8_t)30     // [0... 255]
#define TOFF_VALUE          (uint8_t)4      // [1... 15]
#define R_SENSE             (float)0.11f    // Match to your driver. SilentStepStick series use 0.11

#define IDX_PIN             (uint8_t)2      // TMC2209 Index Output (one pulse per each four fullsteps)
#define DIAG_PIN            (uint8_t)3      // TMC2209 Diagnostics Output (HIGH = error)
#define EN_PIN              (uint8_t)4      // Enable
#define SW_RX               (uint8_t)5      // TMC2209 SoftwareSerial receive pin
#define SW_TX               (uint8_t)6      // TMC2209 SoftwareSerial transmit pin
#define STEP_PIN            (uint8_t)7      // Step
#define DIR_PIN             (uint8_t)8      // Direction
#define VIO_PIN             (uint8_t)9      // I/O power to TMC2209
#define LEDRED_PIN          (uint8_t)10     // Red LED
#define LEDGRN_PIN          (uint8_t)11     // Green LED
#define OW_PIN              (uint8_t)12     // One Wire Bus
#define PWR_MON_PIN         (uint8_t)A7     // Power monitor

/*! \brief Settings to be stored in EEPROM */
struct Settings
{
    /*! \brief Device Id */
    uint32_t    deviceId                = 0;
    /*! \brief Stepper RMS current (mA). */
    uint16_t    rmsCurrent              = 0;
    /*! \brief Maximum stepper position. */
    uint32_t    maxPosition             = 0;
    /*! \brief Maximum stepper speed. */
    uint16_t    maxStepSpeed            = 0;
    /*! \brief Stepper acceleration value. */
    uint16_t    acceleration            = 0;
    /*! \brief Stepper micro steps (power of two exponent). */
    uint8_t     microSteps              = 0;
    /*! \brief Auto homing speed. */
    int16_t     homingSpeed             = 0;
    /*! \brief Auto homing stall values. */
    uint16_t    homingStallValues       = 0;
    /*! \brief Auto homing timeout value (seconds). */
    uint8_t     homingTimeout           = 0;
    /*! \brief Overshoot value. */
    uint16_t    overShoot               = 0;
    /*! \brief Flags. */
    uint8_t     flags                   = 0;
    /*! \brief Checksum. */
    uint8_t     checksum                = 0;
};

/*! \brief Run levels. */
enum RunLevel : uint8_t
{
    RUN_INIT,
    RUN_LOWVOLT,
    RUN_AUTOHOME,
    RUN_NORMAL,
    RUN_ERROR
};

/*! \brief Auto homing stages. */
enum AutoHome : uint8_t
{
    AH_INIT,
    AH_RUN_OUT,
    AH_RUN_IN,
    AH_RUN_HOME,
    AH_COMPLETED
};

/*! \brief The main BullsEye Focuser Actuator class. */
class BullsEyeFocuserActuator : private AccelStepper, private TMC2209Stepper
{
private:
    bool                             powerOk();
    uint32_t                         readVcc();             // mV
    void                             initStepperDriver();
    void                             tuneStepperDriver();
    void                             resetStepperDriver();
    void                             registerCommands();
    void                             loadSettings();
    void                             saveSettings();
    static void                      diagInterrupt();
    static void                      idxInterrupt();
    void                             setRunLevel(const uint8_t &runLevel);

public:
    BullsEyeFocuserActuator(void(*send_callback)(const uint8_t &count, const uint8_t *buf));
    virtual ~BullsEyeFocuserActuator();

    void                             update();
    void                             reset();
    void                             error(const uint16_t &param);

    uint8_t                          getRunLevel();

    uint32_t                         getDeviceId();
    uint16_t                         getRmsCurrent();
    uint32_t                         getCurPosition();
    uint32_t                         getCurTarget();
    int8_t                           getMotorStatus();
    uint8_t                          getMicroSteps();
    uint32_t                         getMaxPosition();
    uint16_t                         getStepSpeed();
    float                            getTemperature();
    uint8_t                          getDriverStatus();
    uint16_t                         getAcceleration();
    uint16_t                         getHomingSpeed();
    uint16_t                         getHomingStallValues();
    uint8_t                          getHomingTimeout();
    uint16_t                         getOverShoot();
    uint8_t                          getFlags();
    uint32_t                         getPowerStatus();

    void                             setRmsCurrent(const uint16_t &param);
    void                             setPosition(const uint32_t &param);
    void                             setMicroSteps(const uint8_t &param);
    void                             setMaxPosition(const uint32_t &param);
    void                             setStepSpeed(const uint16_t &param);
    void                             setAcceleration(const uint16_t &param);
    void                             setHomingSpeed(const int16_t &param);
    void                             setHomingStallValues(const uint16_t &param);
    void                             setHomingTimeout(const uint8_t &param);
    void                             setOverShoot(const uint16_t &param);
    void                             setFlags(const uint8_t &param);

    void                             retLimitStatus();

    void                             stop();
    void                             setNewTarget(const uint32_t &param);
    void                             goToTarget();
    void                             engage(const uint8_t &param);

    /*! \brief Instance of HoHoo class for USB communications. */
    HoHoo                            m_usbComms;

public:
    // Actions
    /*! \brief Pointer to an instance of CB_stop. */
    CB_stop                         *cb_stop;
    /*! \brief Pointer to an instance of CB_goToTarget. */
    CB_goToTarget                   *cb_goToTarget;
    /*! \brief Pointer to an instance of CB_reset. */
    CB_reset                        *cb_reset;
    /*! \brief Pointer to an instance of CB_engage. */
    CB_engage                       *cb_engage;

    // Requests (Settings)
    /*! \brief Pointer to an instance of CB_getDeviceId. */
    CB_getDeviceId                  *cb_getDeviceId;
    /*! \brief Pointer to an instance of CB_getSettings. */
    CB_getSettings                  *cb_getSettings;

    // Requests (Periodic)
    /*! \brief Pointer to an instance of CB_getUpdate. */
    CB_getUpdate                    *cb_getUpdate;

    // Responses
    /*! \brief Pointer to an instance of CB_error. */
    CB_error                        *cb_error;

    // Settings
    /*! \brief Pointer to an instance of CB_setPos. */
    CB_setPos                       *cb_setPos;
    /*! \brief Pointer to an instance of CB_setNewTarget. */
    CB_setNewTarget                 *cb_setNewTarget;
    /*! \brief Pointer to an instance of CB_setMicroSteps. */
    CB_setMicroSteps                *cb_setMicroSteps;
    /*! \brief Pointer to an instance of CB_setMaxPosition. */
    CB_setMaxPosition               *cb_setMaxPosition;
    /*! \brief Pointer to an instance of CB_setStepSpeed. */
    CB_setStepSpeed                 *cb_setStepSpeed;
    /*! \brief Pointer to an instance of CB_setAcceleration. */
    CB_setAcceleration              *cb_setAcceleration;
    /*! \brief Pointer to an instance of CB_setHomingSpeed. */
    CB_setHomingSpeed               *cb_setHomingSpeed;
    /*! \brief Pointer to an instance of CB_setHomingStallValues. */
    CB_setHomingStallValues         *cb_setHomingStallValues;
    /*! \brief Pointer to an instance of CB_setHomingTimeout. */
    CB_setHomingTimeout             *cb_setHomingTimeout;
    /*! \brief Pointer to an instance of CB_setOverShoot. */
    CB_setOverShoot                 *cb_setOverShoot;
    /*! \brief Pointer to an instance of CB_setRmsCurrent. */
    CB_setRmsCurrent                *cb_setRmsCurrent;
    /*! \brief Pointer to an instance of CB_setFlags. */
    CB_setFlags                     *cb_setFlags;

    // Actions
    /*! \brief Pointer to an instance of class Command (usb_cmd_stop). */
    Command                         *usb_cmd_stop;
    /*! \brief Pointer to an instance of class Command (usb_cmd_goToTarget). */
    Command                         *usb_cmd_goToTarget;
    /*! \brief Pointer to an instance of class Command (usb_cmd_focus). */
    Command                         *usb_cmd_focus;
    /*! \brief Pointer to an instance of class Command (usb_cmd_reset). */
    Command                         *usb_cmd_reset;
    /*! \brief Pointer to an instance of class Command (usb_cmd_engage). */
    Command                         *usb_cmd_engage;

    // Requests (Settings)
    /*! \brief Pointer to an instance of class Command (usb_cmd_getDeviceId). */
    Command                         *usb_cmd_getDeviceId;
    /*! \brief Pointer to an instance of class Command (usb_cmd_getSettings). */
    Command                         *usb_cmd_getSettings;

    // Requests (Update)
    /*! \brief Pointer to an instance of class Command (usb_cmd_getUpdate). */
    Command                         *usb_cmd_getUpdate;

    // Responses
    /*! \brief Pointer to an instance of class Command (usb_cmd_error). */
    Command                         *usb_cmd_error;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retDeviceId). */
    Command                         *usb_cmd_retDeviceId;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retCurPosition). */
    Command                         *usb_cmd_retCurPosition;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retCurTarget). */
    Command                         *usb_cmd_retCurTarget;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retMotorStatus). */
    Command                         *usb_cmd_retMotorStatus;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retMicroSteps). */
    Command                         *usb_cmd_retMicroSteps;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retMaxPosition). */
    Command                         *usb_cmd_retMaxPosition;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retStepSpeed). */
    Command                         *usb_cmd_retStepSpeed;
    /*! \brief Pointer to an instance of class Command (usb_cmd_minLimit). */
    Command                         *usb_cmd_minLimit;
    /*! \brief Pointer to an instance of class Command (usb_cmd_maxLimit). */
    Command                         *usb_cmd_maxLimit;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retTemperature). */
    Command                         *usb_cmd_retTemperature;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retDriverStatus). */
    Command                         *usb_cmd_retDriverStatus;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retAcceleration). */
    Command                         *usb_cmd_retAcceleration;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retRunLevel). */
    Command                         *usb_cmd_retRunLevel;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retHomingSpeed). */
    Command                         *usb_cmd_retHomingSpeed;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retHomingStallValues). */
    Command                         *usb_cmd_retHomingStallValues;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retHomingTimeout). */
    Command                         *usb_cmd_retHomingTimeout;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retOverShoot). */
    Command                         *usb_cmd_retOverShoot;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retRmsCurrent). */
    Command                         *usb_cmd_retRmsCurrent;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retFlags). */
    Command                         *usb_cmd_retFlags;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retPowerStatus). */
    Command                         *usb_cmd_retPowerStatus;
    /*! \brief Pointer to an instance of class Command (usb_cmd_retTmcDiag). */
    Command                         *usb_cmd_retTmcDiag;

    // Settings
    /*! \brief Pointer to an instance of class Command (usb_cmd_setPos). */
    Command                         *usb_cmd_setPos;
    /*! \brief Pointer to an instance of class Command (usb_cmd_setNewTarget). */
    Command                         *usb_cmd_setNewTarget;
    /*! \brief Pointer to an instance of class Command (usb_cmd_setMicroSteps). */
    Command                         *usb_cmd_setMicroSteps;
    /*! \brief Pointer to an instance of class Command (usb_cmd_setMaxPosition). */
    Command                         *usb_cmd_setMaxPosition;
    /*! \brief Pointer to an instance of class Command (usb_cmd_setStepSpeed). */
    Command                         *usb_cmd_setStepSpeed;
    /*! \brief Pointer to an instance of class Command (usb_cmd_setAcceleration). */
    Command                         *usb_cmd_setAcceleration;
    /*! \brief Pointer to an instance of class Command (usb_cmd_setHomingSpeed). */
    Command                         *usb_cmd_setHomingSpeed;
    /*! \brief Pointer to an instance of class Command (usb_cmd_setHomingStallValues). */
    Command                         *usb_cmd_setHomingStallValues;
    /*! \brief Pointer to an instance of class Command (usb_cmd_setHomingTimeout). */
    Command                         *usb_cmd_setHomingTimeout;
    /*! \brief Pointer to an instance of class Command (usb_cmd_setOverShoot). */
    Command                         *usb_cmd_setOverShoot;
    /*! \brief Pointer to an instance of class Command (usb_cmd_setRmsCurrent). */
    Command                         *usb_cmd_setRmsCurrent;
    /*! \brief Pointer to an instance of class Command (usb_cmd_setFlags). */
    Command                         *usb_cmd_setFlags;


private:
    void                             runInit();
    void                             runLowVolt();
    void                             runAutoHome();
    void                             runNormal();
    void                             runError();

private:
    /*! \brief The current acceleration value. */
    uint16_t                         m_acceleration;
    /*! \brief The current micro steps value (power of two exponent). */
    uint8_t                          m_microSteps;
    /*! \brief The absolute maximum position measured by auto homing routine. */
    uint32_t                         m_absoluteMaxPosition;
    /*! \brief The maximum position exposed to the user. */
    uint32_t                         m_maxPosition;
    /*! \brief The current stepping speed value. */
    uint16_t                         m_maxStepSpeed;
    /*! \brief The current target position. */
    uint32_t                         m_curTarget;
    /*! \brief The current temperature value. */
    float                            m_temperature;
    /*! \brief The min limit value. */
    uint8_t                          m_minLimit;
    /*! \brief The max limit value. */
    uint8_t                          m_maxLimit;
    /*! \brief The last min limit value. */
    uint8_t                          m_lastMinLimit;
    /*! \brief The last max limit value. */
    uint8_t                          m_lastMaxLimit;
    /*! \brief The current homing speed value. */
    int16_t                          m_homingSpeed;
    /*! \brief The current outward motion homing stall value. */
    uint8_t                          m_homingStallOutValue;
    /*! \brief The current inward motion homing stall value. */
    uint8_t                          m_homingStallInValue;
    /*! \brief The current homing timeout value. */
    uint8_t                          m_homingTimeout;
    /*! \brief The current homing marigin value. */
    uint16_t                         m_homingMarigin;
    /*! \brief The overshoot user setting value. */
    uint16_t                         m_overShoot;
    /*! \brief The current flags. */
    uint8_t                          m_flags;
    /*! \brief The current run level value. */
    uint8_t                          m_runLevel;
    /*! \brief The current stepper driver communication status value. */
    int8_t                           m_driverCommsStatus;
    /*! \brief The last update time in micro seconds. */
    unsigned long                    m_lastUpdate;
    /*! \brief The current timeout counter value. */
    unsigned long                    m_timeoutCount;  
    /*! \brief The current auto homing stage status. */
    uint8_t                          m_ahStatus;
    /*! \brief The current active overshoot value. */
    uint32_t                         m_curOverShoot;
    /*! \brief The stepper motor RMS current value. */
    uint16_t                         m_rmsCurrent;
    /*! \brief The raw analogue input value from power measurement. */
    int                              m_pwrRaw;
    /*! \brief The current error code value. */
    uint16_t                         m_error;
    
    /*! \brief Pointer to an instance of OneWire used by DallasTemperature class. */
    OneWire                         *m_oneWire;
    /*! \brief Pointer to an instance of DallasTemperature class for temperature measurement. */
    DallasTemperature               *m_dallasTemperature;

    /*! \brief Index counter incremented by interrupt routine. */
    static volatile uint32_t         s_idxCount;
    /*! \brief Flag to ignore or react to TMC2209 diag pulses. */
    static volatile bool             s_diagEnable;
    /*! \brief Stores the pointer to BullsEyeFocuserActuator instance so that static interrupt functions can access it. */
    static volatile BullsEyeFocuserActuator *s_bef;
};

#endif
