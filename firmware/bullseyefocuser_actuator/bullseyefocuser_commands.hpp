/*****************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef BULLSEYEFOCUSER_COMMANDS
#define BULLSEYEFOCUSER_COMMANDS

/*!
@file
\brief Contains command and command parameter data type definitions
*/

#include <stdint.h>

#include "hohoo.hpp"

/*! \brief Bitmask for Use Auto Home flag. */
#define FLAG_USEAUTOHOME    (uint8_t)0b00000001

/*! \brief Bitmask for Invert Direction flag. */
#define FLAG_INVERTDIR      (uint8_t)0b00000010

/*! \brief Bitmask for Use Acceleration flag. */
#define FLAG_USEACCEL       (uint8_t)0b00000100

/*! \brief Error message codes. */
enum Error : uint8_t 
{
    UNKNOWN             = 0x00,
    DRIVER_COMMS        = 0x01,
    AH_TIMEOUT          = 0x02,
    MEMORY              = 0x03
};

/*!
\brief Enumeration of device serial commands.
*/
enum BullsEyeFocuserCmd : uint8_t 
{
    // Actions
    stop_C                      = 0x10,
    goToTarget_C                = 0x11,
    reset_C                     = 0x12,
    engage_C                    = 0x13,

    // Requests (Settings)
    getDeviceId_C               = 0x30,
    getSettings_C               = 0x31,
    // Requests (Periodic)
    getUpdate_C                 = 0x32,

    // Responses
    error_C                     = 0x50,
    retDeviceId_C               = 0x51,
    retCurPosition_C            = 0x52,
    retCurTarget_C              = 0x53,
    retMotorStatus_C            = 0x54,
    retMicroSteps_C             = 0x55,
    retMaxPosition_C            = 0x56,
    retStepSpeed_C              = 0x57,
    retTemperature_C            = 0x58,
    minLimit_C                  = 0x59,
    maxLimit_C                  = 0x5A,
    retDriverStatus_C           = 0x5B,
    retAcceleration_C           = 0x5C,
    retRunLevel_C               = 0x5D,
    retHomingSpeed_C            = 0x5E,
    retHomingStallValues_C      = 0x5F,
    retHomingTimeout_C          = 0x60,
    retOverShoot_C              = 0x61,
    retRmsCurrent_C             = 0x62,
    retFlags_C                  = 0x63,
    retPowerStatus_C            = 0x64,
    retTmcDiag_C                = 0x65,

    // Settings
    setPos_C                    = 0x70,
    setNewTarget_C              = 0x71,
    setMicroSteps_C             = 0x72,
    setMaxPosition_C            = 0x73,
    setStepSpeed_C              = 0x74,
    setAcceleration_C           = 0x75,
    setHomingSpeed_C            = 0x76,
    setHomingStallValues_C      = 0x77,
    setHomingTimeout_C          = 0x78,
    setOverShoot_C              = 0x79,
    setRmsCurrent_C             = 0x7A,
    setFlags_C                  = 0x7B,
};

/*!
\brief Enumeration of device serial command data types.
*/
enum BullsEyeFocuserCmdT : uint8_t
{
    // Actions
    stop_T                      = VOID_T,
    goToTarget_T                = VOID_T,
    reset_T                     = VOID_T,
    engage_T                    = UINT8_T,

    // Requests (Settings)
    getDeviceId_T               = VOID_T,
    getSettings_T               = VOID_T,
    // Requests (Periodic)
    getUpdate_T                 = VOID_T,

    // Responses
    error_T                     = UINT16_T,
    retDeviceId_T               = UINT32_T,
    retCurPosition_T            = UINT32_T,
    retCurTarget_T              = UINT32_T,
    retMotorStatus_T            = INT8_T,
    retMicroSteps_T             = UINT8_T,
    retMaxPosition_T            = UINT32_T,
    retStepSpeed_T              = UINT16_T,
    retTemperature_T            = FLOAT_T,
    retDriverStatus_T           = UINT8_T,
    retAcceleration_T           = UINT16_T,
    retRunLevel_T               = UINT8_T,
    retHomingSpeed_T            = INT16_T,
    retHomingStallValues_T      = UINT16_T,
    retHomingTimeout_T          = UINT8_T,
    retOverShoot_T              = UINT16_T,
    retRmsCurrent_T             = UINT16_T,
    retFlags_T                  = UINT8_T,
    retPowerStatus_T            = UINT32_T,
    retTmcDiag_T                = VOID_T,

    // Status
    minLimit_T                  = UINT8_T,
    maxLimit_T                  = UINT8_T,

    // Settings
    setPos_T                    = UINT32_T,
    setNewTarget_T              = UINT32_T,
    setMicroSteps_T             = UINT8_T,
    setMaxPosition_T            = UINT32_T,
    setStepSpeed_T              = UINT16_T,
    setAcceleration_T           = UINT16_T,
    setHomingSpeed_T            = INT16_T,
    setHomingStallValues_T      = UINT16_T,
    setHomingTimeout_T          = UINT8_T,
    setOverShoot_T              = UINT16_T,
    setRmsCurrent_T             = UINT16_T,
    setFlags_T                  = UINT8_T,
};

#endif