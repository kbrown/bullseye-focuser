/*****************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*****************************************************************************/

#include "command.hpp"
#include "hohoo.hpp"

/*!
\brief Constructor

@param parent Pointer to an instance of class HoHoo
*/
Command::Command(HoHoo *parent) :
    m_parent(parent),
    m_cmd(0xFF),
    m_param_type(0xFF)
{

}

/*!
\brief Unregisters (disables) this command.

This will only set the internal command and parameter type to a special value of 0xFF (disabled).
*/
void Command::unRegisterCommand()
{
    m_cmd = 0xFF;
    m_param_type = 0xFF;
}

/*!
\brief Initializes this command.

Called by Command::registerCommand()

@param cmd The command number.
@param param_type Data type of the command's parameter.

\sa DataTypes
*/
void Command::initCommand(const uint8_t &cmd, const uint8_t &param_type)
{
    m_cmd = cmd;
    m_param_type = param_type;
}

/*!
\brief Registers this command.

This version is for commands without a callback.

@param cmd The command number.
@param param_type Data type of the command's parameter.

\return true if succesful
\return false if not succesful
*/
bool Command::registerCommand(const uint8_t &cmd, const uint8_t &param_type)
{
    if(m_cmd != 0xFF)
        return false;

    initCommand(cmd, param_type);
    
    return true;        
}

/*!
\brief Calls the appropriate call back function.

Called by HoHoo::update()

@param buf Buffer containing parameter data for the command.
*/
void Command::run(const uint8_t *buf)
{
    if(m_cmd != 0xFF)
    {
        switch(m_param_type)
        {
        case VOID_T:
            if(m_callback)
            {
                static_cast<CallbackBase<void>*>(m_callback)->run();
            }
            break;
        case INT8_T:
            if(m_callback)
            {
                int8_t i8;
                CommandUtils::fromBytes(buf, i8);
                static_cast<CallbackBase<int8_t>*>(m_callback)->run(i8);
            }
            break;
        case UINT8_T:
            if(m_callback)
            {
                uint8_t ui8;
                CommandUtils::fromBytes(buf, ui8);
                static_cast<CallbackBase<uint8_t>*>(m_callback)->run(ui8);
            }
            break;
        case INT16_T:
            if(m_callback)
            {
                int16_t i16;
                CommandUtils::fromBytes(buf, i16);
                static_cast<CallbackBase<int16_t>*>(m_callback)->run(i16);
            }
            break;
        case UINT16_T:
            if(m_callback)
            {
                uint16_t ui16;
                CommandUtils::fromBytes(buf, ui16);
                static_cast<CallbackBase<uint16_t>*>(m_callback)->run(ui16);
            }
            break;
        case INT32_T:
            if(m_callback)
            {
                int32_t i32;
                CommandUtils::fromBytes(buf, i32);
                static_cast<CallbackBase<int32_t>*>(m_callback)->run(i32);
            }
            break;
        case UINT32_T:
            if(m_callback)
            {
                uint32_t ui32;
                CommandUtils::fromBytes(buf, ui32);
                static_cast<CallbackBase<uint32_t>*>(m_callback)->run(ui32);
            }
            break;
        case FLOAT_T:
            if(m_callback)
            {
                float f;
                CommandUtils::fromBytes(buf, f);
                static_cast<CallbackBase<float>*>(m_callback)->run(f);
            }
            break;
        }
    }
}

/*!
\brief Prepares the internal buffer.

@param buf Pointer to the buffer.
@param ml Message length.

\return Checksum of the initialized buffer.
*/
uint8_t Command::initBuffer(uint8_t *buf, const uint8_t &ml)
{
    const uint8_t cs = SB + ml + m_cmd + m_param_type + EB;

    buf[0] = SB;
    buf[1] = ml;
    buf[2] = m_cmd;
    buf[3] = m_param_type;
    buf[ml - 2] = EB;
    return cs;
}

/*!
\brief Prepares buffer for sending.

This version is for commands without a parameter.

@param buf Pointer to the buffer.
@param message_len Message length.
*/
void Command::asStream(uint8_t *buf, uint8_t &message_len)
{
    const uint8_t ml = 6;
    uint8_t cs = initBuffer(buf, ml);
    buf[5] = cs;

    message_len = ml;
}

/*!
\brief Calls the send callback of this command.

This version is for commands without a parameter.
*/
void Command::send()
{
    if(m_parent)
    {
        uint8_t bytes;
        asStream(m_parent->m_buffer, bytes);
        if(m_parent->m_send_callback)
            m_parent->m_send_callback(bytes, m_parent->m_buffer);
    }
}

/*!
\brief Calls the send callback of this command.

This version is for commands with a parameter.

@param param The parameter to the command.
*/
template <typename T>
void Command::send(const T &param)
{
    if(m_parent)    
    {
        uint8_t bytes;
        asStream(m_parent->m_buffer, param, bytes);
        if(m_parent->m_send_callback)
            m_parent->m_send_callback(bytes, m_parent->m_buffer);
    }
}

template void Command::send<int8_t>(const int8_t &param);
template void Command::send<uint8_t>(const uint8_t &param);
template void Command::send<int16_t>(const int16_t &param);
template void Command::send<uint16_t>(const uint16_t &param);
template void Command::send<int32_t>(const int32_t &param);
template void Command::send<uint32_t>(const uint32_t &param);
template void Command::send<float>(const float &param);

/*!
\brief Converts param to a byte stream.

@param buf Buffer to store the bytes in.
@param param The parameter to be converted.
@param cs Checksum parameter will be updated accordingly.
*/
template <typename T>
void CommandUtils::toBytes(uint8_t *buf, const T &param, uint8_t &cs)
{
    uint8_t bytes = sizeof(param);
    for(uint8_t i = 0; i < bytes; i++)
    {
        uint8_t idx = bytes - i - 1;
        buf[idx] = param >> (i * 8);
        cs += buf[idx];
    }
}

/*!
\brief Converts param to a byte stream.

Specialized float version.

@param buf Buffer to store the bytes in.
@param param The parameter to be converted.
@param cs Checksum parameter will be updated accordingly.
*/
template <>
void CommandUtils::toBytes<float>(uint8_t *buf, const float &param, uint8_t &cs)
{
    #ifdef __AVR__
        long int asInt = *((long int*)&param);
    #else
        int asInt = *((int*)&param);
    #endif

    uint8_t bytes = sizeof(float);
    for(unsigned int i = 0; i < bytes; i++)
    {
        uint8_t idx = bytes - i - 1;
        buf[idx] = (asInt >> (i * 8)) & 0xFF;
        cs += buf[idx];
    }
}

template void CommandUtils::toBytes<int8_t>(uint8_t *buf, const int8_t &param, uint8_t &cs);
template void CommandUtils::toBytes<uint8_t>(uint8_t *buf, const uint8_t &param, uint8_t &cs);
template void CommandUtils::toBytes<int16_t>(uint8_t *buf, const int16_t &param, uint8_t &cs);
template void CommandUtils::toBytes<uint16_t>(uint8_t *buf, const uint16_t &param, uint8_t &cs);
template void CommandUtils::toBytes<int32_t>(uint8_t *buf, const int32_t &param, uint8_t &cs);
template void CommandUtils::toBytes<uint32_t>(uint8_t *buf, const uint32_t &param, uint8_t &cs);
template void CommandUtils::toBytes<float>(uint8_t *buf, const float &param, uint8_t &cs);

/*!
\brief Converts a byte stream to a param value of type T.

@param buf The buffer of bytes to be converted.
@param param This param will be updated accordingly. 
*/
template <typename T>
void CommandUtils::fromBytes(const uint8_t *buf, T &param)
{
    uint8_t bytes = sizeof(T);
    param = 0;
    for(uint8_t i = 0; i < bytes; i++)
        param |= (T)buf[bytes - i - 1] << (i * 8);
}

/*!
\brief Converts a byte stream to a param value of type T.

Specialized float version.

@param buf The buffer of bytes to be converted.
@param param This param will be updated accordingly. 
*/
template <>
void CommandUtils::fromBytes<float>(const uint8_t *buf, float &param)
{
    unsigned int val = 0;
    uint8_t bytes = sizeof(float);
    for(unsigned int i = 0; i < bytes; i++)
    {
        uint8_t idx = bytes - i - 1;
        val |= (buf[idx] << 8 * i);
    }
    param = *((float*)&val);
}

template void CommandUtils::fromBytes<int8_t>(const uint8_t *buf, int8_t &param);
template void CommandUtils::fromBytes<uint8_t>(const uint8_t *buf, uint8_t &param);
template void CommandUtils::fromBytes<int16_t>(const uint8_t *buf, int16_t &param);
template void CommandUtils::fromBytes<uint16_t>(const uint8_t *buf, uint16_t &param);
template void CommandUtils::fromBytes<int32_t>(const uint8_t *buf, int32_t &param);
template void CommandUtils::fromBytes<uint32_t>(const uint8_t *buf, uint32_t &param);
template void CommandUtils::fromBytes<float>(const uint8_t *buf, float &param);
