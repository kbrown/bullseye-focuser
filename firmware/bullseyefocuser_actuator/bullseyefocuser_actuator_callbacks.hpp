/*****************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef CALLBACKS_H
#define CALLBACKS_H

#include <stdint.h>

#include "callbackbase.hpp"
#include "command.hpp"

class BullsEyeFocuserActuator;

/*! \brief Common functionality for all callback classes. */
class CB
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB(BullsEyeFocuserActuator *parent) : m_parent(parent)
    {
    }

protected:
    /*!
    \brief Stores the pointer of the parent class BullsEyeFocuserActuator
    */
    BullsEyeFocuserActuator *m_parent;
};



/*! \brief Callback class for stop. */
class CB_stop : public CB, public CallbackBase<void>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_stop(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run();
};



/*! \brief Callback class for goToTarget. */
class CB_goToTarget : public CB, public CallbackBase<void>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_goToTarget(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run();
};



/*! \brief Callback class for reset. */
class CB_reset : public CB, public CallbackBase<void>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_reset(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run();
};



/*! \brief Callback class for engage. */
class CB_engage : public CB, public CallbackBase<uint8_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_engage(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run(const uint8_t &param);
};



/*! \brief Callback class for getDeviceId. */
class CB_getDeviceId : public CB, public CallbackBase<void>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_getDeviceId(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run();
};


/*! \brief Callback class for getSettings. */
class CB_getSettings : public CB, public CallbackBase<void>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_getSettings(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run();
};



/*! \brief Callback class for getUpdate. */
class CB_getUpdate : public CB, public CallbackBase<void>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_getUpdate(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run();
};



/*! \brief Callback class for error. */
class CB_error : public CB, public CallbackBase<uint16_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_error(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run(const uint16_t &param);
};



/*! \brief Callback class for setPos. */
class CB_setPos : public CB, public CallbackBase<uint32_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_setPos(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run(const uint32_t &param);
};



/*! \brief Callback class for setNewTarget. */
class CB_setNewTarget : public CB, public CallbackBase<uint32_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_setNewTarget(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run(const uint32_t &param);
};



/*! \brief Callback class for setMicroSteps. */
class CB_setMicroSteps : public CB, public CallbackBase<uint8_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_setMicroSteps(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run(const uint8_t &param);
};



/*! \brief Callback class for setMaxPosition. */
class CB_setMaxPosition : public CB, public CallbackBase<uint32_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_setMaxPosition(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run(const uint32_t &param);
};



/*! \brief Callback class for setStepSpeed. */
class CB_setStepSpeed : public CB, public CallbackBase<uint16_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_setStepSpeed(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run(const uint16_t &param);
};



/*! \brief Callback class for setAcceleration. */
class CB_setAcceleration : public CB, public CallbackBase<uint16_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_setAcceleration(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run(const uint16_t &param);
};



/*! \brief Callback class for setHomingSpeed. */
class CB_setHomingSpeed : public CB, public CallbackBase<int16_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_setHomingSpeed(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run(const int16_t &param);
};



/*! \brief Callback class for setHomingStallValue. */
class CB_setHomingStallValues : public CB, public CallbackBase<uint16_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_setHomingStallValues(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run(const uint16_t &param);
};



/*! \brief Callback class for setHomingTimeout. */
class CB_setHomingTimeout : public CB, public CallbackBase<uint8_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_setHomingTimeout(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run(const uint8_t &param);
};



/*! \brief Callback class for setOverShoot. */
class CB_setOverShoot : public CB, public CallbackBase<uint16_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_setOverShoot(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run(const uint16_t &param);
};



/*! \brief Callback class for setRmsCurrent. */
class CB_setRmsCurrent : public CB, public CallbackBase<uint16_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_setRmsCurrent(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run(const uint16_t &param);
};



/*! \brief Callback class for setFlags. */
class CB_setFlags : public CB, public CallbackBase<uint8_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_setFlags(BullsEyeFocuserActuator *parent) : CB(parent)
    {
    }

    void run(const uint8_t &param);
};

#endif