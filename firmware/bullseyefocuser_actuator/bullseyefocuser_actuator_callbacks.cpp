/*****************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*****************************************************************************/

#include <stdio.h>

#include "bullseyefocuser_actuator.hpp"
#include "bullseyefocuser_actuator_callbacks.hpp"



/*! \brief CB_stop boiler plate */
void CB_stop::run()
{
    m_parent->stop();
}

/*! \brief CB_goToTarget boiler plate */
void CB_goToTarget::run()
{
    m_parent->goToTarget();
}

/*! \brief CB_reset boiler plate */
void CB_reset::run()
{
    m_parent->reset();
}

/*! \brief CB_engage boiler plate */
void CB_engage::run(const uint8_t &param)
{
    m_parent->engage(param);
}


/*! \brief CB_getDeviceId boiler plate */
void CB_getDeviceId::run()
{
    m_parent->usb_cmd_retDeviceId->send(m_parent->getDeviceId());
}

/*! \brief CB_getSettings boiler plate */
void CB_getSettings::run()
{
    m_parent->usb_cmd_retDeviceId->send(m_parent->getDeviceId());
    m_parent->usb_cmd_retRunLevel->send(m_parent->getRunLevel());
    m_parent->usb_cmd_retMicroSteps->send(m_parent->getMicroSteps());
    m_parent->usb_cmd_retMaxPosition->send(m_parent->getMaxPosition());
    m_parent->usb_cmd_retStepSpeed->send(m_parent->getStepSpeed());
    m_parent->usb_cmd_retAcceleration->send(m_parent->getAcceleration());
    m_parent->usb_cmd_retHomingSpeed->send(m_parent->getHomingSpeed());
    m_parent->usb_cmd_retHomingStallValues->send(m_parent->getHomingStallValues());
    m_parent->usb_cmd_retHomingTimeout->send(m_parent->getHomingTimeout());
    m_parent->usb_cmd_retOverShoot->send(m_parent->getOverShoot());
    m_parent->usb_cmd_retRmsCurrent->send(m_parent->getRmsCurrent());
    m_parent->usb_cmd_retFlags->send(m_parent->getFlags());
}

void CB_getUpdate::run()
{
    int ms = m_parent->getMotorStatus();
    if(ms == 0)
    {
        m_parent->usb_cmd_retRunLevel->send(m_parent->getRunLevel());
        m_parent->usb_cmd_retCurPosition->send(m_parent->getCurPosition());
        m_parent->usb_cmd_retCurTarget->send(m_parent->getCurTarget());
        m_parent->usb_cmd_retMotorStatus->send(m_parent->getMotorStatus());
        m_parent->usb_cmd_retTemperature->send(m_parent->getTemperature());
        m_parent->usb_cmd_retDriverStatus->send(m_parent->getDriverStatus());
        m_parent->usb_cmd_retPowerStatus->send(m_parent->getPowerStatus());
        m_parent->retLimitStatus();
    }
} 

/*! \brief CB_error boiler plate */
void CB_error::run(const uint16_t &param)
{
    m_parent->error(param);
}

/*! \brief CB_setPos boiler plate */
void CB_setPos::run(const uint32_t &param)
{
    m_parent->setPosition(param);
}

/*! \brief CB_setNewTarget boiler plate */
void CB_setNewTarget::run(const uint32_t &param)
{
    m_parent->setNewTarget(param);
}

/*! \brief CB_setMicroSteps boiler plate */
void CB_setMicroSteps::run(const uint8_t &param)
{
    m_parent->setMicroSteps(param);
}

/*! \brief CB_setMaxPosition boiler plate */
void CB_setMaxPosition::run(const uint32_t &param)
{
    m_parent->setMaxPosition(param);
}

/*! \brief CB_setStepSpeed boiler plate */
void CB_setStepSpeed::run(const uint16_t &param)
{
    m_parent->setStepSpeed(param);
}

/*! \brief CB_setAcceleration boiler plate */
void CB_setAcceleration::run(const uint16_t &param)
{
    m_parent->setAcceleration(param);
}

/*! \brief CB_setHomingSpeed boiler plate */
void CB_setHomingSpeed::run(const int16_t &param)
{
    m_parent->setHomingSpeed(param);
}

/*! \brief CB_setHomingStallValue boiler plate */
void CB_setHomingStallValues::run(const uint16_t &param)
{
    m_parent->setHomingStallValues(param);
}

/*! \brief CB_setHomingTimeout boiler plate */
void CB_setHomingTimeout::run(const uint8_t &param)
{
    m_parent->setHomingTimeout(param);
}

/*! \brief CB_setOverShoot boiler plate */
void CB_setOverShoot::run(const uint16_t &param)
{
    m_parent->setOverShoot(param);
}

/*! \brief CB_setRmsCurrent boiler plate */
void CB_setRmsCurrent::run(const uint16_t &param)
{
    m_parent->setRmsCurrent(param);
}

/*! \brief CB_setFlags boiler plate */
void CB_setFlags::run(const uint8_t &param)
{
    m_parent->setFlags(param);
}
