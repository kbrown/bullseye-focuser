#!/usr/bin/env python3

import os, sys, time, serial

import logging
import logging.handlers

families = {1: "focuser", 2: "filterwheel", 3: "auxiliary", 4: "weather"}
focusers = {1: "primefocus", 2: "bullseye"}
filterwheels = {1: "frankenwheely"}
auxiliary = {1: "lanternfish"}
weather = {1: "mothsqm"}

syslog = logging.getLogger('syslogger')
syslog.setLevel(logging.INFO)

handler = logging.handlers.SysLogHandler(address = '/dev/log')
syslog.addHandler(handler)

def getDeviceId(ttyPort):
    syslog.info("hohoo_probe: getDeviceId(%s) begin..." % ttyPort)

    # Open the serial port
    try:
        ser = serial.Serial(port = "/dev/%s" % ttyPort,
                            baudrate = 9600,
                            parity = serial.PARITY_NONE,
                            stopbits = serial.STOPBITS_ONE,
                            bytesize = serial.EIGHTBITS,
                            timeout = 1)
    except:
        syslog.critical("hohoo_probe: Could not open serial port: %s" % ttyPort)
        return None

    syslog.info("hohoo_probe: Serial port opened...")

    # Give the device some time to initialize
    time.sleep(5)

    # See if we're getting too much data.
    if ser.in_waiting > 200:
        syslog.critical("hohoo_probe: Device too chatty: %d" % ser.in_waiting)
        return None

    syslog.info("hohoo_probe: Flushing serial comms...")

    # Read the bytes available to flush the buffer
    if ser.in_waiting > 0:
        ret = ser.read(ser.in_waiting)
        syslog.info(' '.join('{:02X}'.format(x) for x in ret))

    syslog.info("hohoo_probe: Requesting Device Id...")

    # Send Device Id request command
    cmdGetDeviceId = bytes([0xD2, 0x06, 0x30, 0x00, 0xF0, 0xF8])
    ser.write(cmdGetDeviceId)

    # Give the device some time to respond
    time.sleep(1)

    # Response should be 10 bytes
    ret = None
    ret = ser.read(10)

    syslog.info("hohoo_probe: Response:")
    syslog.info(' '.join('{:02X}'.format(x) for x in ret))

    if(len(ret) < 10):
        syslog.critical("hohoo_probe: Received less then 10 bytes.")
        return None

    # Check the received data
    cs = 0
    family = 0
    device = 0
    header = bytes([0xD2, 0x0A, 0x51, 0x06])
    for i in range(9):
        # Add to the check sum
        cs += ret[i]

        # Check the header
        if(i < 4):
            if(ret[i] != header[i]):
                syslog.critical("hohoo_probe: Received header didn't match.")
                return None

        # Read the device family data
        if(i == 4):
            family |= ret[i] << 8
        if(i == 5):
            family |= ret[i]

        # Read the device number
        if(i == 6):
            device |= ret[i] << 8
        if(i == 7):
            device |= ret[i]

    # Check the check sum
    cs &= 0xFF
    if(cs != ret[9]):
        syslog.critical("hohoo_probe: Checksum error.")
        return None

    syslog.info("hohoo_probe: Device family = %d, device = %d" % (family, device))

    # Return the device family and device numbers
    return family, device


def getSymlinkName(family, device):
    name = None

    if not family in families:
        syslog.critical("hohoo_probe: Unknown device family.")
        return None

    family = families[family]
    name = "hohoo_%s" % family

    if(family == "focuser"):
        if device in focusers:
            name += "_%s" % focusers[device]
        else:
            syslog.critical("hohoo_probe: Unknown focuser.")
            return None
    elif(family == "filterwheel"):
        if device in filterwheels:
            name += "_%s" % filterwheels[device]
        else:
            syslog.critical("hohoo_probe: Unknown filterwheel.")
            return None
    elif(family == "auxiliary"):
        if device in auxiliary:
            name += "_%s" % auxiliary[device]
        else:
            syslog.critical("hohoo_probe: Unknown auxiliary device.")
            return None
    elif(family == "weather"):
        if device in weather:
            name += "_%s" % weather[device]
        else:
            syslog.critical("hohoo_probe: Unknown weather device.")
            return None

    files = os.listdir("%sdev" % os.path.sep)

    n = 0
    while True:
        symlink = "%s%d" % (name, n)
        if symlink not in files:
            return symlink
        n += 1


if __name__ == "__main__":
    if(sys.argv[1] == "add"):
        ret = getDeviceId(sys.argv[2])
        if(ret == None):
            syslog.critical("hohoo_probe: Unknown device.")
            sys.exit(1)
        
        symlink = getSymlinkName(ret[0], ret[1])
        if(symlink == None):
            syslog.critical("hohoo_probe: Error generating symlink name")
            sys.exit(1)

        symlink = os.path.join("%sdev" % os.path.sep, symlink)
        target = os.path.join("%sdev" % os.path.sep, sys.argv[2])
        try:
            os.symlink(target, symlink)
            syslog.info("hohoo_probe: Created symlink: %s -> %s" % (symlink, target))
        except:
            syslog.critical("hohoo_probe: Error creating symlink: %s -> %s" % (symlink, target))
            sys.exit(1)

    if(sys.argv[1] == "remove"):
        files = os.listdir("%sdev" % os.path.sep)

        for f in files:
            ff = os.path.join("%sdev" % os.path.sep, f)
            if os.path.islink(ff):
                tf = os.readlink(ff)
                if sys.argv[2] in tf and "hohoo_" in ff:
                    try:
                        os.remove(ff)
                        syslog.info("hohoo_probe: Deleted symlink: %s -> %s" % (ff, tf))
                    except:
                        syslog.critical("hohoo_probe: Error deleting symlink: %s -> %s" % (ff, tf))
                        sys.exit(1)

    sys.exit(0)
