<?xml version="1.0" encoding="UTF-8"?>
<driversList>
<devGroup group="Focusers">
   <device label="BullsEyeFocuser">
      <driver name="BullsEyeFocuser">indi_bullseye_focus</driver>
      <version>@BULLSEYEFOCUSER_VERSION_MAJOR@.@BULLSEYEFOCUSER_VERSION_MINOR@</version>
   </device>
</devGroup>
</driversList>
