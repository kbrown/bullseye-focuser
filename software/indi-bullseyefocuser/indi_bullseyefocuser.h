/*******************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

#pragma once

#include <memory>
#include <string.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "indifocuser.h"
#include "indicom.h"
#include "indipropertytext.h"
#include "indipropertyswitch.h"
#include "indipropertynumber.h"
#include "indipropertylight.h"

#include "hohoo.hpp"
#include "bullseyefocuser_commands.hpp"
#include "indi_bullseyefocuser_callbacks.hpp"

#define POLLDELAY 1000
#define READTIMEOUT 1

/*!
\brief Utilities outside the BullsEyeFocuser class scope
*/
namespace BullsEyeFocuserUtils
{
    void sendCallback(const uint8_t &count, const uint8_t *buf);
};

/*! \brief The main BullsEyeFocuser class. */
class BullsEyeFocuser : public INDI::Focuser
{
public:
    BullsEyeFocuser();
    virtual ~BullsEyeFocuser();

    const char *getDefaultName();

    bool initProperties();
    void ISGetProperties(const char *dev);
    bool updateProperties();

    virtual bool Handshake();
    virtual void TimerHit();

    virtual bool ISNewNumber(const char *dev, const char *name, double values[], char *names[], int n);
    virtual bool ISNewSwitch(const char *dev, const char *name, ISState *states, char *names[], int n);

    virtual IPState MoveAbsFocuser(uint32_t ticks);
    virtual IPState MoveRelFocuser(FocusDirection dir, uint32_t ticks);
    virtual bool AbortFocuser();

    void sendToActuator(const uint8_t &count, const uint8_t *buf);

    void error(const uint16_t &param);
    void retDeviceId(const uint32_t &param);
    void retCurPosition(const uint32_t &param);
    void retCurTarget(const uint32_t &param);
    void retMotorStatus(const int8_t &param);
    void retMicroSteps(const uint8_t &param);
    void retMaxPosition(const uint32_t &param);
    void retStepSpeed(const uint16_t &param);
    void retTemperature(const float &param);
    void minLimit(const uint8_t &param);
    void maxLimit(const uint8_t &param);
    void retDriverStatus(const uint8_t &param);
    void retAcceleration(const uint16_t &param);
    void retRunLevel(const uint8_t &param);
    void retHomingSpeed(const uint16_t &param);
    void retHomingStallValues(const uint16_t &param);
    void retHomingTimeout(const uint8_t &param);
    void retOverShoot(const uint16_t &param);
    void retRmsCurrent(const uint16_t &param);
    void retFlags(const uint8_t &param);
    void retPowerStatus(const uint32_t &param);
    void retTmcDiag();

private:
    /*!
    \brief Stores an instance of class HoHoo for serial communications.
    */
    HoHoo   usbComms;

    void    registerCommands();
    bool    initFocuser();
    void    readActuator();

    // Response Callbacks
    /*! \brief Pointer to an instance of CB_error. */
    CB_error                    *cb_error;
    /*! \brief Pointer to an instance of CB_retDeviceId. */
    CB_retDeviceId              *cb_retDeviceId;
    /*! \brief Pointer to an instance of CB_retCurPosition. */
    CB_retCurPosition           *cb_retCurPosition;
    /*! \brief Pointer to an instance of CB_retCurTarget. */
    CB_retCurTarget             *cb_retCurTarget;
    /*! \brief Pointer to an instance of CB_retMotorStatus. */
    CB_retMotorStatus           *cb_retMotorStatus;
    /*! \brief Pointer to an instance of CB_retMicroSteps. */
    CB_retMicroSteps            *cb_retMicroSteps;
    /*! \brief Pointer to an instance of CB_retMaxPosition. */
    CB_retMaxPosition           *cb_retMaxPosition;
    /*! \brief Pointer to an instance of CB_retStepSpeed. */
    CB_retStepSpeed             *cb_retStepSpeed;
    /*! \brief Pointer to an instance of CB_retTemperature. */
    CB_retTemperature           *cb_retTemperature;
    /*! \brief Pointer to an instance of CB_minLimit. */
    CB_minLimit                 *cb_minLimit;
    /*! \brief Pointer to an instance of CB_maxLimit. */
    CB_maxLimit                 *cb_maxLimit;
    /*! \brief Pointer to an instance of CB_retDriverStatus. */
    CB_retDriverStatus          *cb_retDriverStatus;
    /*! \brief Pointer to an instance of CB_retAcceleration. */
    CB_retAcceleration          *cb_retAcceleration;
    /*! \brief Pointer to an instance of CB_retRunLevel. */
    CB_retRunLevel              *cb_retRunLevel;
    /*! \brief Pointer to an instance of CB_retHomingSpeed. */
    CB_retHomingSpeed           *cb_retHomingSpeed;
    /*! \brief Pointer to an instance of CB_retHomingStallValue. */
    CB_retHomingStallValues     *cb_retHomingStallValues;
    /*! \brief Pointer to an instance of CB_retHomingTimeout. */
    CB_retHomingTimeout         *cb_retHomingTimeout;
    /*! \brief Pointer to an instance of CB_retOverShoot. */
    CB_retOverShoot             *cb_retOverShoot;
    /*! \brief Pointer to an instance of CB_retRmsCurrent. */
    CB_retRmsCurrent            *cb_retRmsCurrent;
    /*! \brief Pointer to an instance of CB_retFlags. */
    CB_retFlags                 *cb_retFlags;
    /*! \brief Pointer to an instance of CB_retPowerStatus. */
    CB_retPowerStatus           *cb_retPowerStatus;
    /*! \brief Pointer to an instance of CB_retTmcDiag. */
    CB_retTmcDiag               *cb_retTmcDiag;

    // Actions
    /*! \brief Pointer to an instance of class Command (cmd_stop). */
    Command                     *cmd_stop;
    /*! \brief Pointer to an instance of class Command (cmd_goToTarget). */
    Command                     *cmd_goToTarget;
    /*! \brief Pointer to an instance of class Command (cmd_reset). */
    Command                     *cmd_reset;
    /*! \brief Pointer to an instance of class Command (cmd_engage). */
    Command                     *cmd_engage;

    // Requests (Settings)
    /*! \brief Pointer to an instance of class Command (cmd_getSettings). */
    Command                     *cmd_getSettings;

    // Requests (Periodic Update)
    /*! \brief Pointer to an instance of class Command (cmd_getUpdate). */
    Command                     *cmd_getUpdate;

    // Responses
    /*! \brief Pointer to an instance of class Command (cmd_error). */
    Command                     *cmd_error;
    /*! \brief Pointer to an instance of class Command (cmd_retDeviceId). */
    Command                     *cmd_retDeviceId;
    /*! \brief Pointer to an instance of class Command (cmd_retCurPosition). */
    Command                     *cmd_retCurPosition;
    /*! \brief Pointer to an instance of class Command (cmd_retCurTarget). */
    Command                     *cmd_retCurTarget;
    /*! \brief Pointer to an instance of class Command (cmd_retMotorStatus). */
    Command                     *cmd_retMotorStatus;
    /*! \brief Pointer to an instance of class Command (cmd_retMicroSteps). */
    Command                     *cmd_retMicroSteps;
    /*! \brief Pointer to an instance of class Command (cmd_retMaxPosition). */
    Command                     *cmd_retMaxPosition;
    /*! \brief Pointer to an instance of class Command (cmd_retStepSpeed). */
    Command                     *cmd_retStepSpeed;
    /*! \brief Pointer to an instance of class Command (cmd_retTemperature). */
    Command                     *cmd_retTemperature;
    /*! \brief Pointer to an instance of class Command (cmd_minLimit). */
    Command                     *cmd_minLimit;
    /*! \brief Pointer to an instance of class Command (cmd_maxLimit). */
    Command                     *cmd_maxLimit;
    /*! \brief Pointer to an instance of class Command (cmd_retDriverStatus). */
    Command                     *cmd_retDriverStatus;
    /*! \brief Pointer to an instance of class Command (cmd_retAcceleration). */
    Command                     *cmd_retAcceleration;
    /*! \brief Pointer to an instance of class Command (cmd_retRunLevel). */
    Command                     *cmd_retRunLevel;
    /*! \brief Pointer to an instance of class Command (cmd_retHomingSpeed). */
    Command                     *cmd_retHomingSpeed;
    /*! \brief Pointer to an instance of class Command (cmd_retHomingStallValue). */
    Command                     *cmd_retHomingStallValues;
    /*! \brief Pointer to an instance of class Command (cmd_retHomingTimeout). */
    Command                     *cmd_retHomingTimeout;
    /*! \brief Pointer to an instance of class Command (cmd_retOverShoot). */
    Command                     *cmd_retOverShoot;
    /*! \brief Pointer to an instance of class Command (cmd_retRmsCurrent). */
    Command                     *cmd_retRmsCurrent;
    /*! \brief Pointer to an instance of class Command (cmd_retFlags). */
    Command                     *cmd_retFlags;
    /*! \brief Pointer to an instance of class Command (cmd_retPowerStatus). */
    Command                     *cmd_retPowerStatus;
    /*! \brief Pointer to an instance of class Command (cmd_retTmcDiag). */
    Command                     *cmd_retTmcDiag;

    // Settings
    /*! \brief Pointer to an instance of class Command (cmd_setPos). */
    Command                     *cmd_setPos;
    /*! \brief Pointer to an instance of class Command (cmd_setNewTarget). */
    Command                     *cmd_setNewTarget;
    /*! \brief Pointer to an instance of class Command (cmd_setMicroSteps). */
    Command                     *cmd_setMicroSteps;
    /*! \brief Pointer to an instance of class Command (cmd_setMaxPosition). */
    Command                     *cmd_setMaxPosition;
    /*! \brief Pointer to an instance of class Command (cmd_setStepSpeed). */
    Command                     *cmd_setStepSpeed;
    /*! \brief Pointer to an instance of class Command (cmd_setAcceleration). */
    Command                     *cmd_setAcceleration;
    /*! \brief Pointer to an instance of class Command (cmd_setHomingSpeed). */
    Command                     *cmd_setHomingSpeed;
    /*! \brief Pointer to an instance of class Command (cmd_setHomingStallValue). */
    Command                     *cmd_setHomingStallValues;
    /*! \brief Pointer to an instance of class Command (cmd_setHomingTimeout). */
    Command                     *cmd_setHomingTimeout;
    /*! \brief Pointer to an instance of class Command (cmd_setOverShoot). */
    Command                     *cmd_setOverShoot;
    /*! \brief Pointer to an instance of class Command (cmd_setRmsCurrent). */
    Command                     *cmd_setRmsCurrent;
    /*! \brief Pointer to an instance of class Command (cmd_setFlags). */
    Command                     *cmd_setFlags;

    /*! \brief Device Id Text Property */
    INDI::PropertyText DeviceIdTP{1};

    /*! \brief Engage Switch Property */
    INDI::PropertySwitch EngageSP{2};
    
    /*! \brief Reset Switch Property*/
    INDI::PropertySwitch ResetSP{1};

    /*!
    \brief Enumeration of microsteps
    */
    enum MicroSteps
    {
        MS_1,
        MS_2,
        MS_4,
        MS_8,
        MS_16,
        MS_32,
        MS_64,
        MS_128,
        MS_256,
        MS_COUNT
    };

    /*! \brief Microsteps Switch Property*/
    INDI::PropertySwitch MicroStepsSP{MS_COUNT};

    /*! \brief Step speed Number Property*/
    INDI::PropertyNumber StepSpeedNP{1};

    /*! \brief Motor Status Light Property*/
    INDI::PropertyLight MotorStatusLP{3};

    /*! \brief Limit Status Light Property*/
    INDI::PropertyLight LimitStatusLP{2};

    /*! \brief Current Target Number Property*/
    INDI::PropertyNumber CurrentTargetNP{1};

    /*! \brief Temperature Number Property*/
    INDI::PropertyNumber TemperatureNP{1};

    /*! \brief Set Position (without moving motor) Number Property*/
    INDI::PropertyNumber SetPositionNP{1};

    /*! \brief Acceleration Number Property*/
    INDI::PropertyNumber AccelerationNP{1};

    /*!
    \brief Enumeration of run levels
    */
    enum RunLevel
    {
        RUN_INIT,
        RUN_LOWVOLT,
        RUN_AUTOHOME,
        RUN_NORMAL,
        RUN_ERROR
    };

    /*! \brief Run Level Light Property Light Property*/
    INDI::PropertyLight RunLevelLP{5};

    /*! \brief Homing Speed Number Property*/
    INDI::PropertyNumber HomingSpeedNP{1};

    /*! \brief Homing Stall Value for outward movement Number Property*/
    INDI::PropertyNumber HomingStallOutValueNP{1};

    /*! \brief Homing Stall Value for inward movement Number Property*/
    INDI::PropertyNumber HomingStallInValueNP{1};

    /*! \brief Homing Timeout Number Property*/
    INDI::PropertyNumber HomingTimeoutNP{1};

    /*! \brief OverShoot Number Property*/
    INDI::PropertyNumber OverShootNP{1};

    /*! \brief Rms Current Number Property*/
    INDI::PropertyNumber RmsCurrentNP{1};

    /*! \brief Use Auto Home Switch Property*/
    INDI::PropertySwitch UseAutoHomeSP{2};

    /*! \brief Use Acceleration Switch Property*/
    INDI::PropertySwitch UseAccelerationSP{2};

    /*! \brief Invert Direction Switch Property*/
    INDI::PropertySwitch InvertDirSP{2};

    /*! \brief Homing Stall Value for inward movement*/
    uint8_t m_homingStallInValue;

    /*! \brief Homing Stall Value for outward movement*/
    uint8_t m_homingStallOutValue;

    /*! \brief Run level */
    uint8_t m_runLevel;

    /*! \brief Flags */
    uint8_t m_flags;

    /*! \brief Power Status Number Property*/
    INDI::PropertyNumber PowerStatusNP{1};

    /*! \brief Device Id*/
    char m_deviceId[10];
};