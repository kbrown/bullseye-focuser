/*******************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef CALLBACKS_H
#define CALLBACKS_H

#include <stdint.h>

#include "callbackbase.hpp"
#include "command.hpp"

class BullsEyeFocuser;

/*! \brief Common functionality for all callback classes. */
class CB
{
public:
    /*!
    \brief Constructor

    @param parent The parent class.
    */
    CB(BullsEyeFocuser *parent) : m_parent(parent)
    {
    }

    virtual ~CB() {};

protected:
    /*!
    \brief Stores the pointer of the parent class BullsEyeFocuser
    */
    BullsEyeFocuser *m_parent;
};

/*! \brief Callback class for error. */
class CB_error : public CB, public CallbackBase<uint16_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_error(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_error() {};

    void run(const uint16_t &param);
};

/*! \brief Callback class for retDeviceId. */
class CB_retDeviceId : public CB, public CallbackBase<uint32_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retDeviceId(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retDeviceId() {};

    void run(const uint32_t &param);
};

/*! \brief Callback class for retCurPosition. */
class CB_retCurPosition : public CB, public CallbackBase<uint32_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retCurPosition(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retCurPosition() {};

    void run(const uint32_t &param);
};

/*! \brief Callback class for retCurTarget. */
class CB_retCurTarget : public CB, public CallbackBase<uint32_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retCurTarget(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retCurTarget() {};

    void run(const uint32_t &param);
};

/*! \brief Callback class for retMotorStatus. */
class CB_retMotorStatus : public CB, public CallbackBase<int8_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retMotorStatus(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retMotorStatus() {};

    void run(const int8_t &param);
};

/*! \brief Callback class for retMicroSteps. */
class CB_retMicroSteps : public CB, public CallbackBase<uint8_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retMicroSteps(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retMicroSteps() {};

    void run(const uint8_t &param);
};

/*! \brief Callback class for retMaxPosition. */
class CB_retMaxPosition : public CB, public CallbackBase<uint32_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retMaxPosition(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retMaxPosition() {};

    void run(const uint32_t &param);
};

/*! \brief Callback class for retStepSpeed. */
class CB_retStepSpeed : public CB, public CallbackBase<uint16_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retStepSpeed(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retStepSpeed() {};

    void run(const uint16_t &param);
};

/*! \brief Callback class for retTemperature. */
class CB_retTemperature : public CB, public CallbackBase<float>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retTemperature(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retTemperature() {};

    void run(const float &param);
};

/*! \brief Callback class for minLimit. */
class CB_minLimit : public CB, public CallbackBase<uint8_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_minLimit(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_minLimit() {};

    void run(const uint8_t &param);
};

/*! \brief Callback class for maxLimit. */
class CB_maxLimit : public CB, public CallbackBase<uint8_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_maxLimit(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_maxLimit() {};

    void run(const uint8_t &param);
};

/*! \brief Callback class for retDriverStatus. */
class CB_retDriverStatus : public CB, public CallbackBase<uint8_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retDriverStatus(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retDriverStatus() {};

    void run(const uint8_t &param);
};

/*! \brief Callback class for retAcceleration. */
class CB_retAcceleration : public CB, public CallbackBase<uint16_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retAcceleration(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retAcceleration() {};

    void run(const uint16_t &param);
};

/*! \brief Callback class for retRunLevel. */
class CB_retRunLevel : public CB, public CallbackBase<uint8_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retRunLevel(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retRunLevel() {};

    void run(const uint8_t &param);
};

/*! \brief Callback class for retHomingSpeed. */
class CB_retHomingSpeed : public CB, public CallbackBase<uint16_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retHomingSpeed(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retHomingSpeed() {};

    void run(const uint16_t &param);
};

/*! \brief Callback class for retHomingStallValues. */
class CB_retHomingStallValues : public CB, public CallbackBase<uint16_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retHomingStallValues(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retHomingStallValues() {};

    void run(const uint16_t &param);
};

/*! \brief Callback class for retHomingTimeout. */
class CB_retHomingTimeout : public CB, public CallbackBase<uint8_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retHomingTimeout(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retHomingTimeout() {};

    void run(const uint8_t &param);
};

/*! \brief Callback class for retOverShoot. */
class CB_retOverShoot : public CB, public CallbackBase<uint16_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retOverShoot(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retOverShoot() {};

    void run(const uint16_t &param);
};

/*! \brief Callback class for retRmsCurrent. */
class CB_retRmsCurrent : public CB, public CallbackBase<uint16_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retRmsCurrent(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retRmsCurrent() {};

    void run(const uint16_t &param);
};

/*! \brief Callback class for retFlags. */
class CB_retFlags : public CB, public CallbackBase<uint8_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retFlags(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retFlags() {};

    void run(const uint8_t &param);
};

/*! \brief Callback class for retPowerStatus. */
class CB_retPowerStatus : public CB, public CallbackBase<uint32_t>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retPowerStatus(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retPowerStatus() {};

    void run(const uint32_t &param);
};

/*! \brief Callback class for retTmcDiag. */
class CB_retTmcDiag : public CB, public CallbackBase<void>
{
public:
    /*!
    \brief Constructor

    @param parent The parent class
    */
    CB_retTmcDiag(BullsEyeFocuser *parent) : CB(parent)
    {
    }

    virtual ~CB_retTmcDiag() {};

    void run();
};
#endif