/*******************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

#include "indi_bullseyefocuser.h"

std::unique_ptr<BullsEyeFocuser> bullsEyeFocuser(new BullsEyeFocuser());

/*!
\brief Needed for HoHoo. Actual send happens in BullsEyeFocuser::sendToActuator()
*/
void BullsEyeFocuserUtils::sendCallback(const uint8_t &count, const uint8_t *buf)
{
    bullsEyeFocuser->sendToActuator(count, buf);
}

/*!
\brief BullsEyeFocuser constructor.

Initializes run level. Instantiates callbacks. Registers commands. Sets the send callback.
*/
BullsEyeFocuser::BullsEyeFocuser() : m_runLevel(RunLevel::RUN_INIT)
{
    FI::SetCapability(FOCUSER_CAN_ABS_MOVE | FOCUSER_CAN_REL_MOVE | FOCUSER_CAN_ABORT);

    cb_error = new CB_error(this);
    cb_retDeviceId = new CB_retDeviceId(this);
    cb_retCurPosition = new CB_retCurPosition(this);
    cb_retCurTarget = new CB_retCurTarget(this);
    cb_retMotorStatus = new CB_retMotorStatus(this);
    cb_retMicroSteps = new CB_retMicroSteps(this);
    cb_retMaxPosition = new CB_retMaxPosition(this);
    cb_retStepSpeed = new CB_retStepSpeed(this);
    cb_retTemperature = new CB_retTemperature(this);
    cb_minLimit = new CB_minLimit(this);
    cb_maxLimit = new CB_maxLimit(this);
    cb_retDriverStatus = new CB_retDriverStatus(this);
    cb_retAcceleration = new CB_retAcceleration(this);
    cb_retRunLevel = new CB_retRunLevel(this);
    cb_retHomingSpeed = new CB_retHomingSpeed(this);
    cb_retHomingStallValues = new CB_retHomingStallValues(this);
    cb_retHomingTimeout = new CB_retHomingTimeout(this);
    cb_retOverShoot = new CB_retOverShoot(this);
    cb_retRmsCurrent = new CB_retRmsCurrent(this);
    cb_retFlags = new CB_retFlags(this);
    cb_retPowerStatus = new CB_retPowerStatus(this);
    cb_retTmcDiag = new CB_retTmcDiag(this);

    snprintf(m_deviceId, 10, "0000:0000");

    registerCommands();
    usbComms.setSendCallback(BullsEyeFocuserUtils::sendCallback);
}

/*!
\brief BullsEyeFocuser destructor.

Deletes callbacks.
*/
BullsEyeFocuser::~BullsEyeFocuser()
{
    delete cb_error;
    delete cb_retDeviceId;
    delete cb_retCurPosition;
    delete cb_retCurTarget;
    delete cb_retMotorStatus;
    delete cb_retMicroSteps;
    delete cb_retMaxPosition;
    delete cb_retStepSpeed;
    delete cb_retTemperature;
    delete cb_minLimit;
    delete cb_maxLimit;
    delete cb_retDriverStatus;
    delete cb_retAcceleration;
    delete cb_retRunLevel;
    delete cb_retHomingSpeed;
    delete cb_retHomingStallValues;
    delete cb_retHomingTimeout;
    delete cb_retOverShoot;
    delete cb_retRmsCurrent;
    delete cb_retFlags;
    delete cb_retPowerStatus;
    delete cb_retTmcDiag;
}

/*!
\brief Sends data to the focuser actuator.

Sends the data in the buffer over USB to the focuser actuator.

@param count Number of bytes to send 
@param buf Pointer to the buffer containing the data.
*/
void BullsEyeFocuser::sendToActuator(const uint8_t &count, const uint8_t *buf)
{
    int nbytes_written = 0, rc = -1;
    char errstr[MAXRBUF];

    tcflush(PortFD, TCIOFLUSH);

    // DEBUGF(INDI::Logger::DBG_SESSION, "Writing %d bytes", (int)count);
    if((rc = tty_write(PortFD, (char*)buf, count, &nbytes_written)) != TTY_OK)
    {
        tty_error_msg(rc, errstr, MAXRBUF);
        DEBUGF(INDI::Logger::DBG_ERROR, "sendToActuator() write error: %s.", errstr);
    }
}

/*!
\brief Perform handshake with device to check communication.

\return true if successful
\return false if not successful
*/
bool BullsEyeFocuser::Handshake()
{
    DEBUG(INDI::Logger::DBG_SESSION, "BullsEyeFocuser hand shake begin...");
    if(initFocuser())
    {
        for(int i = 0; i < 3; i++)
        {
            // DEBUGF(INDI::Logger::DBG_SESSION, "Attempt %d / 3", i + 1);
            sleep(2);
            readActuator();
            // DEBUGF(INDI::Logger::DBG_SESSION, "deviceId: %s", m_deviceId);
            if(!strcmp(m_deviceId, "0001:0002"))
            {
                DEBUG(INDI::Logger::DBG_SESSION, "Hand shake successful...");
                return true;
            }
        }
    }

    DEBUG(INDI::Logger::DBG_SESSION, "Error connecting to BullsEyeFocuser, please ensure BullsEyeFocuser is turned on and the port is correct.");

    sleep(5);
    return false;
}

/*!
\return Name of the device
*/
const char *BullsEyeFocuser::getDefaultName()
{
    return (char *)"BullsEye Focuser";
}

/*!
\brief Defines default driver properties to the client. Called by the INDI framework.

@param dev Device name.
*/
void BullsEyeFocuser::ISGetProperties(const char *dev)
{
    // DEBUG(INDI::Logger::DBG_SESSION, "BullsEyeFocuser::ISGetProperties()");
    if(dev && strcmp(dev, getDeviceName()))
        return;

    INDI::Focuser::ISGetProperties(dev);
}

/*!
\brief Initializes BullsEye Focuser specific properties.

This only initializes the properties. It does not define them to the client.

\return true on success
\return false on failure
*/
bool BullsEyeFocuser::initProperties()
{
    INDI::Focuser::initProperties();

    DeviceIdTP[0].fill("id", "Id", "");
    DeviceIdTP.fill(getDeviceName(), "device_id", "Device Id", "Settings", IP_RO, 0, IPS_IDLE);

    ResetSP[0].fill("Reset", "Reset", ISS_OFF);
    ResetSP.fill(getDeviceName(), "Reset", "Reset",
                 MAIN_CONTROL_TAB, IP_RW, ISR_ATMOST1, 60, IPS_IDLE);

    EngageSP[0].fill("en_0", "Engage", ISS_ON);
    EngageSP[1].fill("en_1", "Disengage", ISS_OFF);
    EngageSP.fill(getDeviceName(), "motor", "Motor",
                  MAIN_CONTROL_TAB, IP_RW, ISR_1OFMANY, 60, IPS_BUSY);

    MicroStepsSP[MS_1].fill("ms_1", "Full Step", ISS_ON);
    MicroStepsSP[MS_2].fill("ms_2", "Half Step", ISS_OFF);
    MicroStepsSP[MS_4].fill("ms_4", "Quarter Step", ISS_OFF);
    MicroStepsSP[MS_8].fill("ms_8", "8th Step", ISS_OFF);
    MicroStepsSP[MS_16].fill("ms_16", "16th Step", ISS_OFF);
    MicroStepsSP[MS_32].fill("ms_32", "32nd Step", ISS_OFF);
    MicroStepsSP[MS_64].fill("ms_64", "64th Step", ISS_OFF);
    MicroStepsSP[MS_128].fill("ms_128", "128th Step", ISS_OFF);
    MicroStepsSP[MS_256].fill("ms_256", "256th Step", ISS_OFF);
    MicroStepsSP.fill(getDeviceName(), "MicroSteps", "Micro Steps",
                      "Settings", IP_RW, ISR_1OFMANY, 60, IPS_BUSY);

    StepSpeedNP[0].fill("sps", "Steps Per Sec", "%6.0f", 50, 8000, 500, 0);
    StepSpeedNP.fill(getDeviceName(), "step_speed", "Step Speed", "Settings", IP_RW, 0, IPS_BUSY);

    MotorStatusLP[0].fill("motor_in", "In", IPS_IDLE);
    MotorStatusLP[1].fill("motor_stop", "Stop", IPS_IDLE);
    MotorStatusLP[2].fill("motor_out", "Out", IPS_IDLE);
    MotorStatusLP.fill(getDeviceName(), "motor_status", "Motor Status", MAIN_CONTROL_TAB, IPS_BUSY);

    LimitStatusLP[0].fill("limit_in", "In", IPS_BUSY);
    LimitStatusLP[1].fill("limit_out", "Out", IPS_BUSY);
    LimitStatusLP.fill(getDeviceName(), "limit_status", "Limit Status", MAIN_CONTROL_TAB, IPS_BUSY);

    CurrentTargetNP[0].fill("target", "Target", "%6.0f", 0, 0x7FFFF, 500, 0);
    CurrentTargetNP.fill(getDeviceName(), "current_target", "Current Target", MAIN_CONTROL_TAB, IP_RO, 0, IPS_BUSY);

    TemperatureNP[0].fill("celcius", "Celcius", "%6.2f", -273.15, 100.0, 0.0, -273.15);
    TemperatureNP.fill(getDeviceName(), "temperature", "Temperature", MAIN_CONTROL_TAB, IP_RO, 0, IPS_BUSY);

    SetPositionNP[0].fill("set_pos", "Ticks", "%6.0f", 0, 0x7FFFF, 500, 0);
    SetPositionNP.fill(getDeviceName(), "set_position", "Set Position", "Settings", IP_RW, 0, IPS_BUSY);

    AccelerationNP[0].fill("sps_a", "Steps Per Sec", "%6.0f", 50, 4000, 500, 0);
    AccelerationNP.fill(getDeviceName(), "acceleration", "Acceleration", "Settings", IP_RW, 0, IPS_BUSY);

    RunLevelLP[0].fill("init", "Init", IPS_IDLE);
    RunLevelLP[1].fill("lowvolt", "Low Voltage", IPS_IDLE);
    RunLevelLP[2].fill("autohome", "Auto Home", IPS_IDLE);
    RunLevelLP[3].fill("normal", "Normal", IPS_IDLE);
    RunLevelLP[4].fill("error", "Error", IPS_IDLE);
    RunLevelLP.fill(getDeviceName(), "runlevelLP", "Run Level", MAIN_CONTROL_TAB, IPS_BUSY);

    HomingSpeedNP[0].fill("hms", "Steps Per Sec", "%6.0f", 50, 8000, 500, 0);
    HomingSpeedNP.fill(getDeviceName(), "homingspeed", "Homing Speed", "Settings", IP_RW, 0, IPS_BUSY);

    HomingStallOutValueNP[0].fill("hmsto", "Value", "%6.0f", 0, 255, 5, 127);
    HomingStallOutValueNP.fill(getDeviceName(), "homingstalloutvalue", "Homing Stall Out Value", "Settings", IP_RW, 0, IPS_BUSY);

    HomingStallInValueNP[0].fill("hmsti", "Value", "%6.0f", 0, 255, 5, 127);
    HomingStallInValueNP.fill(getDeviceName(), "homingstallinvalue", "Homing Stall In Value", "Settings", IP_RW, 0, IPS_BUSY);

    HomingTimeoutNP[0].fill("hmto", "Seconds", "%6.0f", 5, 120, 5, 45);
    HomingTimeoutNP.fill(getDeviceName(), "homingtimeout", "Homing Timeout", "Settings", IP_RW, 0, IPS_BUSY);

    OverShootNP[0].fill("ovrs", "Steps", "%6.0f", 0, 500, 50, 0);
    OverShootNP.fill(getDeviceName(), "overshoot", "Overshoot", "Settings", IP_RW, 0, IPS_BUSY);

    RmsCurrentNP[0].fill("rmsc", "mA", "%6.0f", 100, 1770, 50, 0);
    RmsCurrentNP.fill(getDeviceName(), "rmscurrent", "RMS Current", "Settings", IP_RW, 0, IPS_BUSY);

    UseAutoHomeSP[0].fill("ah_1", "On", ISS_ON);
    UseAutoHomeSP[1].fill("ah_0", "Off", ISS_OFF);
    UseAutoHomeSP.fill(getDeviceName(), "ah", "Use Auto Home",
                  "Settings", IP_RW, ISR_1OFMANY, 60, IPS_BUSY);

    InvertDirSP[0].fill("inv_1", "On", ISS_OFF);
    InvertDirSP[1].fill("inv_0", "Off", ISS_ON);
    InvertDirSP.fill(getDeviceName(), "inv", "Invert Direction",
                  "Settings", IP_RW, ISR_1OFMANY, 60, IPS_BUSY);

    UseAccelerationSP[0].fill("acc_1", "On", ISS_ON);
    UseAccelerationSP[1].fill("acc_0", "Off", ISS_OFF);
    UseAccelerationSP.fill(getDeviceName(), "acc", "Use Acceleration",
                  "Settings", IP_RW, ISR_1OFMANY, 60, IPS_BUSY);

    PowerStatusNP[0].fill("volts", "Volts", "%6.2f", 0.0, 20.0, 0.0, 0.0);
    PowerStatusNP.fill(getDeviceName(), "power", "Power", MAIN_CONTROL_TAB, IP_RO, 0, IPS_BUSY);

    FocusRelPosN[0].min   = 0;
    FocusRelPosN[0].max   = 10000;
    FocusRelPosN[0].value = 500;
    FocusRelPosN[0].step  = 500;

    FocusAbsPosN[0].min   = 0;
    FocusAbsPosN[0].max   = 0x7FFFF;
    FocusAbsPosN[0].step  = 1000;
    FocusAbsPosN[0].value = 0;

    FocusMaxPosN[0].min   = 0;
    FocusMaxPosN[0].max   = 0x7FFFF;
    FocusMaxPosN[0].step  = 500;
    FocusMaxPosN[0].value = 0;

    return true;
}

/*!
\brief Defines or deletes properties to the client depending on connection status.

If the device is connected, only a subset of properties will be defined to the client here.
The rest of them are defined in BullsEyeFocuser::retRunLevel depending on the current run level.

\return true on success
\return false on failure
*/
bool BullsEyeFocuser::updateProperties()
{

    if(isConnected())
    {
        // DEBUG(INDI::Logger::DBG_SESSION, "Define properties");
        defineProperty(DeviceIdTP);
        defineProperty(ResetSP);
        defineProperty(RunLevelLP);
        defineProperty(PowerStatusNP);
        defineProperty(TemperatureNP);

        defineProperty(MicroStepsSP);
        defineProperty(RmsCurrentNP);
        defineProperty(StepSpeedNP);
        defineProperty(AccelerationNP);
        defineProperty(HomingSpeedNP);
        defineProperty(HomingStallOutValueNP);
        defineProperty(HomingStallInValueNP);
        defineProperty(HomingTimeoutNP);
        defineProperty(OverShootNP);
        defineProperty(UseAutoHomeSP);
        defineProperty(InvertDirSP);
        defineProperty(UseAccelerationSP);
        defineProperty(SetPositionNP);
    }
    else
    {
        INDI::Focuser::updateProperties();
        deleteProperty(DeviceIdTP.getName());
        deleteProperty(EngageSP.getName());
        deleteProperty(ResetSP.getName());
        deleteProperty(MicroStepsSP.getName());
        deleteProperty(StepSpeedNP.getName());
        deleteProperty(MotorStatusLP.getName());
        deleteProperty(LimitStatusLP.getName());
        deleteProperty(CurrentTargetNP.getName());
        deleteProperty(TemperatureNP.getName());
        deleteProperty(SetPositionNP.getName());
        deleteProperty(AccelerationNP.getName());
        deleteProperty(RunLevelLP.getName());
        deleteProperty(HomingSpeedNP.getName());
        deleteProperty(HomingStallOutValueNP.getName());
        deleteProperty(HomingStallInValueNP.getName());
        deleteProperty(HomingTimeoutNP.getName());
        deleteProperty(OverShootNP.getName());
        deleteProperty(RmsCurrentNP.getName());
        deleteProperty(UseAutoHomeSP.getName());
        deleteProperty(InvertDirSP.getName());
        deleteProperty(UseAccelerationSP.getName());
        deleteProperty(PowerStatusNP.getName());
    }

    return true;
}

/*!
\brief Handles switch property changes from the client.

Updates the client properties and sends appropriate serial commands. Called by the INDI framework.

@param dev Device name.
@param name Property name.
@param states ???
@param names ???
@param n ???

\return true on success
\return false on failure
*/
bool BullsEyeFocuser::ISNewSwitch(const char *dev, const char *name, ISState *states, char *names[], int n)
{
    // DEBUG(INDI::Logger::DBG_SESSION, "BullsEyeFocuser::ISNewSwitch()");
    if(strcmp(dev, getDeviceName()) == 0)
    {
        if(EngageSP.isNameMatch(name))
        {
            EngageSP.update(states, names, n);

            if(MotorStatusLP[1].s != IPS_OK)
            {
                EngageSP.setState(IPS_ALERT);
                // IDSetSwitch(&EngageSP, "Cannot engage/disengage while the motor is running.");
                EngageSP.apply();
                return true;
            }

            uint8_t en = 1;
            int index = EngageSP.findOnSwitchIndex();

            switch (index)
            {
                case 0:
                    en = 1;
                    break;

                case 1:
                    en = 0;
                    break;

                default:
                    EngageSP.setState(IPS_ALERT);
                    // IDSetSwitch(&EngageSP, "Unknown engage setting %d", index);
                    EngageSP.apply();
                    return true;
            }
            cmd_engage->send(en);
            EngageSP.setState(IPS_BUSY);
            // IDSetSwitch(&EngageSP, nullptr);
            EngageSP.apply();
            return true;
        }

        if(MicroStepsSP.isNameMatch(name))
        {
            MicroStepsSP.update(states, names, n);
            
            if(MotorStatusLP[1].s != IPS_OK)
            {
                MicroStepsSP.setState(IPS_ALERT);
                // IDSetSwitch(&MicroStepsSP, "Cannot set micro stepping while the motor is running.");
                MicroStepsSP.apply();
                return true;
            }

            if(FocusAbsPosN[0].value != 0)
            {
                MicroStepsSP.setState(IPS_ALERT);
                // IDSetSwitch(&MicroStepsSP, "Cannot set micro stepping when not in home position.");
                MicroStepsSP.apply();
                return true;
            }

            uint8_t ms = 6;
            int index = MicroStepsSP.findOnSwitchIndex();

            switch (index)
            {
                case MS_1:
                    ms = 0;
                    break;

                case MS_2:
                    ms = 1;
                    break;

                case MS_4:
                    ms = 2;
                    break;

                case MS_8:
                    ms = 3;
                    break;

                case MS_16:
                    ms = 4;
                    break;

                case MS_32:
                    ms = 5;
                    break;

                case MS_64:
                    ms = 6;
                    break;

                case MS_128:
                    ms = 7;
                    break;

                case MS_256:
                    ms = 8;
                    break;

                default:
                    MicroStepsSP.setState(IPS_ALERT);
                    // IDSetSwitch(&MicroStepsSP, "Unknown micro steps index %d", index);
                    MicroStepsSP.apply();
                    return true;
            }
            cmd_setMicroSteps->send(ms);
            MicroStepsSP.setState(IPS_BUSY);
            // IDSetSwitch(&MicroStepsSP, nullptr);
            MicroStepsSP.apply();
            return true;
        }

        if(ResetSP.isNameMatch(name))
        {
            DEBUG(INDI::Logger::DBG_SESSION, "BullsEyeFocuser reset.");

            cmd_reset->send();
            ResetSP.setState(IPS_OK);
            // IDSetSwitch(&ResetSP, nullptr);
            ResetSP.apply();
            return true;
        }

        if(UseAutoHomeSP.isNameMatch(name))
        {
            UseAutoHomeSP.update(states, names, n);

            bool ah = true;
            int index = UseAutoHomeSP.findOnSwitchIndex();

            switch (index)
            {
                case 0:
                    ah = true;
                    break;

                case 1:
                    ah = false;
                    break;

                default:
                    UseAutoHomeSP.setState(IPS_ALERT);
                    UseAutoHomeSP.apply();
                    return true;
            }
            if(ah)
                m_flags |= FLAG_USEAUTOHOME;
            else
                m_flags &= ~FLAG_USEAUTOHOME;
            cmd_setFlags->send(m_flags);
            UseAutoHomeSP.setState(IPS_BUSY);
            UseAutoHomeSP.apply();
            return true;
        }

        if(InvertDirSP.isNameMatch(name))
        {
            InvertDirSP.update(states, names, n);

            bool inv = true;
            int index = InvertDirSP.findOnSwitchIndex();

            switch (index)
            {
                case 0:
                    inv = true;
                    break;

                case 1:
                    inv = false;
                    break;

                default:
                    InvertDirSP.setState(IPS_ALERT);
                    InvertDirSP.apply();
                    return true;
            }
            if(inv)
                m_flags |= FLAG_INVERTDIR;
            else
                m_flags &= ~FLAG_INVERTDIR;
            cmd_setFlags->send(m_flags);
            InvertDirSP.setState(IPS_BUSY);
            InvertDirSP.apply();
            return true;
        }

        if(UseAccelerationSP.isNameMatch(name))
        {
            UseAccelerationSP.update(states, names, n);

            bool acc = true;
            int index = UseAccelerationSP.findOnSwitchIndex();

            switch (index)
            {
                case 0:
                    acc = true;
                    break;

                case 1:
                    acc = false;
                    break;

                default:
                    UseAccelerationSP.setState(IPS_ALERT);
                    UseAccelerationSP.apply();
                    return true;
            }
            if(acc)
                m_flags |= FLAG_USEACCEL;
            else
                m_flags &= ~FLAG_USEACCEL;
            cmd_setFlags->send(m_flags);
            UseAccelerationSP.setState(IPS_BUSY);
            UseAccelerationSP.apply();
            return true;
        }
    }

    return INDI::Focuser::ISNewSwitch(dev, name, states, names, n);
}

/*!
\brief Handles number property changes from the client.

Updates the client properties and sends appropriate serial commands. Called by the INDI framework.

@param dev Device name.
@param name Property name.
@param values ???
@param names ???
@param n ???

\return true on success
\return false on failure
*/
bool BullsEyeFocuser::ISNewNumber(const char *dev, const char *name, double values[], char *names[], int n)
{
    // DEBUG(INDI::Logger::DBG_SESSION, "BullsEyeFocuser::ISNewNumber()");
    if(strcmp(dev, getDeviceName()) == 0)
    {
        if(!strcmp(name, FocusMaxPosNP.name))
        {
            IUUpdateNumber(&FocusMaxPosNP, values, names, n);
            if(MotorStatusLP[1].s != IPS_OK)
            {
                DEBUG(INDI::Logger::DBG_ERROR, "Cannot set maximum position while the motor is running.");
                FocusMaxPosNP.s = IPS_ALERT;
                IDSetNumber(&FocusMaxPosNP, nullptr);
                return true;
            }

            if(FocusAbsPosN[0].value > FocusMaxPosN[0].value)
            {
                DEBUG(INDI::Logger::DBG_ERROR, "Current position is outside the new limit.");
                FocusMaxPosNP.s = IPS_ALERT;
                IDSetNumber(&FocusMaxPosNP, nullptr);
                return true;
            }

            uint32_t max_pos = FocusMaxPosN[0].value;
            cmd_setMaxPosition->send(max_pos);
            FocusMaxPosNP.s = IPS_BUSY;
            IDSetNumber(&FocusMaxPosNP, nullptr);
            return true;
        }

        if(StepSpeedNP.isNameMatch(name))
        {
            StepSpeedNP.update(values, names, n);
            if(MotorStatusLP[1].s != IPS_OK)
            {
                DEBUG(INDI::Logger::DBG_ERROR, "Cannot set step speed while the motor is running.");
                StepSpeedNP.setState(IPS_ALERT);
                StepSpeedNP.apply();
                return true;
            }

            uint16_t speed = StepSpeedNP[0].value;
            cmd_setStepSpeed->send(speed);
            StepSpeedNP.setState(IPS_BUSY);
            StepSpeedNP.apply();
            return true;
        }

        if(SetPositionNP.isNameMatch(name))
        {
            SetPositionNP.update(values, names, n);
            if(MotorStatusLP[1].s != IPS_OK)
            {
                DEBUG(INDI::Logger::DBG_ERROR, "Cannot set motor position while the motor is running.");
                SetPositionNP.setState(IPS_ALERT);
                SetPositionNP.apply();
                return true;
            }

            uint32_t pos = SetPositionNP[0].value;
            cmd_setPos->send(pos);
            cmd_setNewTarget->send(pos);
            SetPositionNP.setState(IPS_BUSY);
            SetPositionNP.apply();
            return true;
        }

        if(AccelerationNP.isNameMatch(name))
        {
            AccelerationNP.update(values, names, n);
            if(MotorStatusLP[1].s != IPS_OK)
            {
                DEBUG(INDI::Logger::DBG_ERROR, "Cannot set acceleration while the motor is running.");
                AccelerationNP.setState(IPS_ALERT);
                AccelerationNP.apply();
                return true;
            }

            uint16_t acc = AccelerationNP[0].value;
            cmd_setAcceleration->send(acc);
            AccelerationNP.setState(IPS_BUSY);
            AccelerationNP.apply();
            return true;
        }

        if(HomingSpeedNP.isNameMatch(name))
        {
            HomingSpeedNP.update(values, names, n);
            uint16_t hms = HomingSpeedNP[0].value;
            cmd_setHomingSpeed->send(hms);
            HomingSpeedNP.setState(IPS_BUSY);
            HomingSpeedNP.apply();
            return true;
        }

        if(HomingStallOutValueNP.isNameMatch(name))
        {
            HomingStallOutValueNP.update(values, names, n);
            m_homingStallOutValue = HomingStallOutValueNP[0].value;
            uint16_t homingStallValues = m_homingStallInValue << 8 | m_homingStallOutValue;
            cmd_setHomingStallValues->send(homingStallValues);
            HomingStallOutValueNP.setState(IPS_BUSY);
            HomingStallOutValueNP.apply();
            return true;
        }

        if(HomingStallInValueNP.isNameMatch(name))
        {
            HomingStallInValueNP.update(values, names, n);
            m_homingStallInValue = HomingStallInValueNP[0].value;
            uint16_t homingStallValues = m_homingStallInValue << 8 | m_homingStallOutValue;
            cmd_setHomingStallValues->send(homingStallValues);
            HomingStallInValueNP.setState(IPS_BUSY);
            HomingStallInValueNP.apply();
            return true;
        }

        if(HomingTimeoutNP.isNameMatch(name))
        {
            HomingTimeoutNP.update(values, names, n);
            uint8_t hms = HomingTimeoutNP[0].value;
            cmd_setHomingTimeout->send(hms);
            HomingTimeoutNP.setState(IPS_BUSY);
            HomingTimeoutNP.apply();
            return true;
        }

        if(OverShootNP.isNameMatch(name))
        {
            OverShootNP.update(values, names, n);
            uint16_t hms = OverShootNP[0].value;
            cmd_setOverShoot->send(hms);
            OverShootNP.setState(IPS_BUSY);
            OverShootNP.apply();
            return true;
        }

        if(RmsCurrentNP.isNameMatch(name))
        {
            RmsCurrentNP.update(values, names, n);
            uint16_t hms = RmsCurrentNP[0].value;
            cmd_setRmsCurrent->send(hms);
            RmsCurrentNP.setState(IPS_BUSY);
            RmsCurrentNP.apply();
            return true;
        }
    }

    return INDI::Focuser::ISNewNumber(dev, name, values, names, n);
}

/*!
\brief Moves the focuser to an absolute position.

@param ticks The absolute position.

\return IPState::IPS_OK on success
\return IPState::IPS_ALERT on failure
*/
IPState BullsEyeFocuser::MoveAbsFocuser(uint32_t ticks)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "MoveAbsFocuser(%d)", (int)ticks);
    if(EngageSP[0].s == ISS_ON)
    {
        cmd_setNewTarget->send(ticks);
        cmd_goToTarget->send();
        return IPS_OK;
    }
    else
    {
        return IPS_ALERT;
    }
}

/*!
\brief Moves the focuser relative to the current position.

@param dir Direction of the move.
@param ticks Number of ticks to move.

\return IPState::IPS_OK on success
\return IPState::IPS_ALERT on failure

\sa BullsEyeFocuser::MoveAbsFocuser
*/
IPState BullsEyeFocuser::MoveRelFocuser(FocusDirection dir, uint32_t ticks)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "MoveRelFocuser(%d, %d)", (int)dir, (int)ticks);
    if(dir == FOCUS_INWARD)
    {
        return MoveAbsFocuser(CurrentTargetNP[0].value - ticks);
    }
    else
    {
        return MoveAbsFocuser(CurrentTargetNP[0].value + ticks);
    }
}

/*!
\brief Aborts the current focuser move.

\return true on success
\return false on failure
*/
bool BullsEyeFocuser::AbortFocuser()
{
    cmd_stop->send();
    return true;
}

/*!
\brief Registers serial commands and their callbacks.
*/
void BullsEyeFocuser::registerCommands()
{
    // Actions
    cmd_stop = usbComms.registerCommand(stop_C, stop_T);
    cmd_goToTarget = usbComms.registerCommand(goToTarget_C, goToTarget_T);
    cmd_reset = usbComms.registerCommand(reset_C, reset_T);
    cmd_engage = usbComms.registerCommand(engage_C, engage_T);

    // Requests (Settings)
    cmd_getSettings = usbComms.registerCommand(getSettings_C, getSettings_T);

    // Requests (Periodic Update)
    cmd_getUpdate = usbComms.registerCommand(getUpdate_C, getUpdate_T);

    // Responses
    cmd_error = usbComms.registerCommand(error_C, error_T, cb_error);
    cmd_retDeviceId = usbComms.registerCommand(retDeviceId_C, retDeviceId_T, cb_retDeviceId);
    cmd_retCurPosition = usbComms.registerCommand(retCurPosition_C, retCurPosition_T, cb_retCurPosition);
    cmd_retCurTarget = usbComms.registerCommand(retCurTarget_C, retCurTarget_T, cb_retCurTarget);
    cmd_retMotorStatus = usbComms.registerCommand(retMotorStatus_C, retMotorStatus_T, cb_retMotorStatus);
    cmd_retMicroSteps = usbComms.registerCommand(retMicroSteps_C, retMicroSteps_T, cb_retMicroSteps);
    cmd_retMaxPosition = usbComms.registerCommand(retMaxPosition_C, retMaxPosition_T, cb_retMaxPosition);
    cmd_retStepSpeed = usbComms.registerCommand(retStepSpeed_C, retStepSpeed_T, cb_retStepSpeed);
    cmd_retTemperature = usbComms.registerCommand(retTemperature_C, retTemperature_T, cb_retTemperature);
    cmd_minLimit = usbComms.registerCommand(minLimit_C, minLimit_T, cb_minLimit);
    cmd_maxLimit = usbComms.registerCommand(maxLimit_C, maxLimit_T, cb_maxLimit);
    cmd_retDriverStatus = usbComms.registerCommand(retDriverStatus_C, retDriverStatus_T, cb_retDriverStatus);
    cmd_retAcceleration = usbComms.registerCommand(retAcceleration_C, retAcceleration_T, cb_retAcceleration);
    cmd_retRunLevel = usbComms.registerCommand(retRunLevel_C, retRunLevel_T, cb_retRunLevel);
    cmd_retHomingSpeed = usbComms.registerCommand(retHomingSpeed_C, retHomingSpeed_T, cb_retHomingSpeed);
    cmd_retHomingStallValues = usbComms.registerCommand(retHomingStallValues_C, retHomingStallValues_T, cb_retHomingStallValues);
    cmd_retHomingTimeout = usbComms.registerCommand(retHomingTimeout_C, retHomingTimeout_T, cb_retHomingTimeout);
    cmd_retOverShoot = usbComms.registerCommand(retOverShoot_C, retOverShoot_T, cb_retOverShoot);
    cmd_retRmsCurrent = usbComms.registerCommand(retRmsCurrent_C, retRmsCurrent_T, cb_retRmsCurrent);
    cmd_retFlags = usbComms.registerCommand(retFlags_C, retFlags_T, cb_retFlags);
    cmd_retPowerStatus = usbComms.registerCommand(retPowerStatus_C, retPowerStatus_T, cb_retPowerStatus);
    cmd_retTmcDiag = usbComms.registerCommand(retTmcDiag_C, retTmcDiag_T, cb_retTmcDiag);

    // Settings
    cmd_setPos = usbComms.registerCommand(setPos_C, setPos_T);
    cmd_setNewTarget = usbComms.registerCommand(setNewTarget_C, setNewTarget_T);
    cmd_setMicroSteps = usbComms.registerCommand(setMicroSteps_C, setMicroSteps_T);
    cmd_setMaxPosition = usbComms.registerCommand(setMaxPosition_C, setMaxPosition_T);
    cmd_setStepSpeed = usbComms.registerCommand(setStepSpeed_C, setStepSpeed_T);
    cmd_setAcceleration = usbComms.registerCommand(setAcceleration_C, setAcceleration_T);
    cmd_setHomingSpeed = usbComms.registerCommand(setHomingSpeed_C, setHomingSpeed_T);
    cmd_setHomingStallValues = usbComms.registerCommand(setHomingStallValues_C, setHomingStallValues_T);
    cmd_setHomingTimeout = usbComms.registerCommand(setHomingTimeout_C, setHomingTimeout_T);
    cmd_setOverShoot = usbComms.registerCommand(setOverShoot_C, setOverShoot_T);
    cmd_setRmsCurrent = usbComms.registerCommand(setRmsCurrent_C, setRmsCurrent_T);
    cmd_setFlags = usbComms.registerCommand(setFlags_C, setFlags_T);
}

/*!
\brief Initialises the focuser device.

Flushes the serial port. Sends requests to get settings and updates from the device. Starts the poll timer.

\return true on success
\return false on failure
*/
bool BullsEyeFocuser::initFocuser()
{
    // DEBUG(INDI::Logger::DBG_SESSION, "BullsEyeFocuser::initFocuser()");
    sleep(2);

    tcflush(PortFD, TCIOFLUSH);

    cmd_getSettings->send();
    // cmd_getUpdate->send();

    SetTimer(POLLDELAY);

    return true;
}

/*!
\brief Reads serial data from the device.
*/
void BullsEyeFocuser::readActuator()
{
    int nbytes_read = 0, rc = -1;
    char errstr[MAXRBUF];
    char resp[2] = {0};
    int bytes_available;

    ioctl(PortFD, FIONREAD, &bytes_available);
    if(bytes_available > 0)
    {
        // DEBUGF(INDI::Logger::DBG_SESSION, "Reading %d bytes", bytes_available);
        rc = tty_read(PortFD, resp, bytes_available, READTIMEOUT, &nbytes_read);
        if(rc != TTY_OK)
        {
            tty_error_msg(rc, errstr, MAXRBUF);
            DEBUGF(INDI::Logger::DBG_ERROR, "Error reading serial port: %s", errstr);
            return;
        }

        for(int i = 0; i < nbytes_read; i++)
        {
            // Seems to be necessary for RPi 2, not sure why...
            usleep(10000);

            // DEBUGF(INDI::Logger::DBG_SESSION, "Received: %02X", (int)resp[i] & 0xFF);
            usbComms.update((uint8_t)resp[i] & 0xFF);
        }
    }
}

/*!
\brief Poll timer callback.

This function gets called after the poll timer (POLLDELAY) has elapsed.
It will read the serial port if there is data available and pass it on
to the usb command parser. It will also restart the poll timer so this
function will be called periodically.
*/
void BullsEyeFocuser::TimerHit()
{
    // DEBUG(INDI::Logger::DBG_SESSION, "BullsEyeFocuser::TimerHit()");

    if(!isConnected())
    {
        SetTimer(POLLDELAY);
        return;
    }

    readActuator();

    if(m_runLevel != RunLevel::RUN_AUTOHOME && m_runLevel != RunLevel::RUN_ERROR)
        cmd_getUpdate->send();

    SetTimer(POLLDELAY);
}

/*!
\brief Error callback.

This function gets called if an error was received from the device.
Currently it is just printed out and no further action is taken.

@param param The error code.
*/
void BullsEyeFocuser::error(const uint16_t &param)
{
    switch (param)
    {
    case Error::UNKNOWN :
        DEBUG(INDI::Logger::DBG_ERROR, "Unknown error.");
        break;

    case Error::DRIVER_COMMS :
        DEBUG(INDI::Logger::DBG_ERROR, "Focuser could not communicate with the stepper driver.");
        break;

    case Error::AH_TIMEOUT :
        DEBUG(INDI::Logger::DBG_ERROR, "Auto Home timeout error.");
        break;

    case Error::MEMORY :
        DEBUG(INDI::Logger::DBG_ERROR, "Focuser could not allocate memory.");
        break;

    default:
        DEBUGF(INDI::Logger::DBG_ERROR, "Focuser error code: %d", (int)param);
        break;
    }
}

/*!
\brief Device Id callback.

This function gets called if Device Id was received from the device.
Corresponding client property will be set accordingly.

@param param The Device Id.
*/
void BullsEyeFocuser::retDeviceId(const uint32_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retDeviceId: %d.", (int)param);

    uint8_t b4 = (param >> 24) & 0xFF;
    uint8_t b3 = (param >> 16) & 0xFF;
    uint8_t b2 = (param >> 8) & 0xFF;
    uint8_t b1 = (param) & 0xFF;
    snprintf(m_deviceId, 10, "%02X%02X:%02X%02X", b4, b3, b2, b1);

    DeviceIdTP[0].setText(m_deviceId);
    DeviceIdTP.setState(IPS_OK);
    DeviceIdTP.apply();
}

/*!
\brief Current Position callback.

This function gets called if Current Position was received from the device.
Corresponding client property will be set accordingly.

@param param Current Position.
*/
void BullsEyeFocuser::retCurPosition(const uint32_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retCurPosition(%d)", (int)param);
    FocusAbsPosN[0].value = param;
    IDSetNumber(&FocusAbsPosNP, nullptr);

    SetPositionNP[0].setValue(param);
    SetPositionNP.apply();

    if(SetPositionNP.getState() != IPS_OK)
    {
        cmd_setNewTarget->send(param);
        SetPositionNP.setState(IPS_OK);
    }
}

/*!
\brief Current Target callback.

This function gets called if Current Target was received from the device.
Corresponding client property will be set accordingly.

@param param Current Target.
*/
void BullsEyeFocuser::retCurTarget(const uint32_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retCurTarget(%d)", (int)param);
    CurrentTargetNP[0].setValue(param);
    CurrentTargetNP.setState(IPS_OK);
    CurrentTargetNP.apply();
}

/*!
\brief Motor Status callback.

This function gets called if Motor Status was received from the device.
Corresponding client property will be set accordingly.

@param param Motor Status. -1 = Moving In. 0 = Stopped. 1 = Moving Out.
*/
void BullsEyeFocuser::retMotorStatus(const int8_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retMotorStatus(%d)", (int)param);
    if(param < 0)
    {
        MotorStatusLP[0].setState(IPS_BUSY);
        MotorStatusLP[1].setState(IPS_IDLE);
        MotorStatusLP[2].setState(IPS_IDLE);
    }
    else if(param == 0)
    {
        MotorStatusLP[0].setState(IPS_IDLE);
        MotorStatusLP[1].setState(IPS_OK);
        MotorStatusLP[2].setState(IPS_IDLE);
    }
    else
    {
        MotorStatusLP[0].setState(IPS_IDLE);
        MotorStatusLP[1].setState(IPS_IDLE);
        MotorStatusLP[2].setState(IPS_BUSY);
    }
    MotorStatusLP.setState(IPS_OK);
    MotorStatusLP.apply();
}

/*!
\brief Micro Steps callback.

This function gets called if Micro Steps was received from the device.
Corresponding client property will be set accordingly.

@param param Micro Steps.
*/
void BullsEyeFocuser::retMicroSteps(const uint8_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retMicroSteps(%d)", (int)param);
    if(MicroStepsSP.getState() != IPS_ALERT)
        MicroStepsSP.setState(IPS_OK);

    MicroStepsSP[MS_1].setState(ISS_OFF);
    MicroStepsSP[MS_2].setState(ISS_OFF);
    MicroStepsSP[MS_4].setState(ISS_OFF);
    MicroStepsSP[MS_8].setState(ISS_OFF);
    MicroStepsSP[MS_16].setState(ISS_OFF);
    MicroStepsSP[MS_32].setState(ISS_OFF);
    MicroStepsSP[MS_64].setState(ISS_OFF);
    MicroStepsSP[MS_128].setState(ISS_OFF);
    MicroStepsSP[MS_256].setState(ISS_OFF);

    switch(param)
    {
    case 0:
        MicroStepsSP[MS_1].setState(ISS_ON);
        break;
    case 1:
        MicroStepsSP[MS_2].setState(ISS_ON);
        break;
    case 2:
        MicroStepsSP[MS_4].setState(ISS_ON);
        break;
    case 3:
        MicroStepsSP[MS_8].setState(ISS_ON);
        break;
    case 4:
        MicroStepsSP[MS_16].setState(ISS_ON);
        break;
    case 5:
        MicroStepsSP[MS_32].setState(ISS_ON);
        break;
    case 6:
        MicroStepsSP[MS_64].setState(ISS_ON);
        break;
    case 7:
        MicroStepsSP[MS_128].setState(ISS_ON);
        break;
    case 8:
        MicroStepsSP[MS_256].setState(ISS_ON);
        break;
    default:
        MicroStepsSP.setState(IPS_ALERT);
    }
    MicroStepsSP.apply();
}

/*!
\brief Maximum Position callback.

This function gets called if Maximum Position was received from the device.
Corresponding client property will be set accordingly.

@param param Maximum Position.
*/
void BullsEyeFocuser::retMaxPosition(const uint32_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retMaxPosition(%d)", (int)param);
    FocusAbsPosN[0].max = param;
    IDSetNumber(&FocusAbsPosNP, nullptr);

    if(FocusMaxPosNP.s != IPS_ALERT)
        FocusMaxPosNP.s = IPS_OK;
    FocusMaxPosN[0].value = param;
    IDSetNumber(&FocusMaxPosNP, nullptr);
}

/*!
\brief Step Speed callback.

This function gets called if Step Speed was received from the device.
Corresponding client property will be set accordingly.

@param param Step Speed.
*/
void BullsEyeFocuser::retStepSpeed(const uint16_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retStepSpeed(%d)", (int)param);
    if(StepSpeedNP.getState() != IPS_ALERT)
        StepSpeedNP.setState(IPS_OK);

    StepSpeedNP[0].setValue(param);
    StepSpeedNP.apply();
}

/*!
\brief Temperature callback.

This function gets called if Temperature was received from the device.
Corresponding client property will be set accordingly.

@param param Temperature.
*/
void BullsEyeFocuser::retTemperature(const float &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retTemperature(%f)", param);

    TemperatureNP[0].setValue(param);
    TemperatureNP.setState(IPS_OK);
    TemperatureNP.apply();
}

/*!
\brief Minimum Limit callback.

This function gets called if Minimum Limit was received from the device.
Corresponding client property will be set accordingly.

@param param Minimum Limit.
*/
void BullsEyeFocuser::minLimit(const uint8_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "minLimit(%d)", param);
    if(param > 0)
        LimitStatusLP[0].setState(IPS_ALERT);
    else
        LimitStatusLP[0].setState(IPS_IDLE);
    LimitStatusLP.setState(IPS_OK);
    LimitStatusLP.apply();
}

/*!
\brief Maximum Limit callback.

This function gets called if Maximum Limit was received from the device.
Corresponding client property will be set accordingly.

@param param Maximum Limit.
*/
void BullsEyeFocuser::maxLimit(const uint8_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "maxLimit(%d)", param);
    if(param > 0)
        LimitStatusLP[1].setState(IPS_ALERT);
    else
        LimitStatusLP[1].setState(IPS_IDLE);
    LimitStatusLP.setState(IPS_OK);
    LimitStatusLP.apply();
}

/*!
\brief Driver Engage Status callback.

This function gets called if Driver Engage Status was received from the device.
Corresponding client property will be set accordingly.

@param param Driver Engage Status. 1 = ON. 0 = OFF.
*/
void BullsEyeFocuser::retDriverStatus(const uint8_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retDriverStatus(%d)", param);
    if(param == 1)
    {
        EngageSP[1].setState(ISS_OFF);
        EngageSP[0].setState(ISS_ON);
    }
    else
    {
        EngageSP[0].setState(ISS_OFF);
        EngageSP[1].setState(ISS_ON);
    }
    EngageSP.setState(IPS_OK);
    EngageSP.apply();
}

/*!
\brief Acceleration callback.

This function gets called if Acceleration was received from the device.
Corresponding client property will be set accordingly.

@param param Acceleration (microsteps / s^2).
*/
void BullsEyeFocuser::retAcceleration(const uint16_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retAcceleration(%d)", param);
    AccelerationNP[0].setValue(param);
    AccelerationNP.setState(IPS_OK);
    AccelerationNP.apply();
}

/*!
\brief Callback to action on run level change.

Sets the correct run level light property. Defines or deletes client properties depending on the run level.

@param param The run level received from the device.
*/
void BullsEyeFocuser::retRunLevel(const uint8_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retRunLevel(%d)", param);
    RunLevelLP[0].setState(IPS_IDLE);
    RunLevelLP[1].setState(IPS_IDLE);
    RunLevelLP[2].setState(IPS_IDLE);
    RunLevelLP[3].setState(IPS_IDLE);
    RunLevelLP[4].setState(IPS_IDLE);

    if(param < RunLevel::RUN_NORMAL)
        RunLevelLP[param].setState(IPS_BUSY);
    else if(param == RunLevel::RUN_ERROR)
        RunLevelLP[param].setState(IPS_ALERT);
    else
        RunLevelLP[param].setState(IPS_OK);

    RunLevelLP.setState(IPS_OK);
    RunLevelLP.apply();

    m_runLevel = param;

    if(m_runLevel == RunLevel::RUN_NORMAL)
    {
        INDI::Focuser::updateProperties();
        defineProperty(EngageSP);
        defineProperty(MotorStatusLP);
        defineProperty(LimitStatusLP);
        defineProperty(CurrentTargetNP);
    }
    else
    {
        // Delete default focuser properties too. This is very hacky (copied from INDI sources)...
        if(FI::CanAbsMove())
        {
            deleteProperty(INDI::Focuser::PresetNP.name);
            deleteProperty(INDI::Focuser::PresetGotoSP.name);
        }

        deleteProperty(FI::FocusMotionSP.name);
        if(FI::HasVariableSpeed())
        {
            deleteProperty(FI::FocusSpeedNP.name);

            if(FI::CanAbsMove() == false)
                deleteProperty(FI::FocusTimerNP.name);
        }
        if(FI::CanRelMove())
            deleteProperty(FI::FocusRelPosNP.name);
        if(FI::CanAbsMove())
        {
            deleteProperty(FI::FocusAbsPosNP.name);
            deleteProperty(FI::FocusMaxPosNP.name);
        }
        if(FI::CanAbort())
            deleteProperty(FI::FocusAbortSP.name);
        if(FI::CanSync())
            deleteProperty(FI::FocusSyncNP.name);
        if(FI::CanReverse())
            deleteProperty(FI::FocusReverseSP.name);
        if(FI::HasBacklash())
        {
            deleteProperty(FI::FocusBacklashSP.name);
            deleteProperty(FI::FocusBacklashNP.name);
        }

        deleteProperty(EngageSP.getName());
        deleteProperty(MotorStatusLP.getName());
        deleteProperty(LimitStatusLP.getName());
        deleteProperty(CurrentTargetNP.getName());
    }
}

/*!
\brief Homing Speed callback.

This function gets called if Homing Speed was received from the device.
Corresponding client property will be set accordingly.

@param param Homing Speed (microsteps / s).
*/
void BullsEyeFocuser::retHomingSpeed(const uint16_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retHomingSpeed(%d)", param);
    HomingSpeedNP[0].setValue(param);
    HomingSpeedNP.setState(IPS_OK);
    HomingSpeedNP.apply();
}

/*!
\brief Homing Stall Value callback.

This function gets called if Homing Stall Value was received from the device.
Corresponding client property will be set accordingly.

@param param Homing Stall Value (0-255).
*/
void BullsEyeFocuser::retHomingStallValues(const uint16_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retHomingStallValue(%d)", param);
    m_homingStallOutValue = param & 0xFF;
    m_homingStallInValue = (param >> 8) & 0xFF;

    HomingStallOutValueNP[0].setValue(m_homingStallOutValue);
    HomingStallOutValueNP.setState(IPS_OK);
    HomingStallOutValueNP.apply();

    HomingStallInValueNP[0].setValue(m_homingStallInValue);
    HomingStallInValueNP.setState(IPS_OK);
    HomingStallInValueNP.apply();
}

/*!
\brief Homing Timeout callback.

This function gets called if Homing Timeout was received from the device.
Corresponding client property will be set accordingly.

@param param Homing Timeout (s).
*/
void BullsEyeFocuser::retHomingTimeout(const uint8_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retHomingTimeout(%d)", param);
    HomingTimeoutNP[0].setValue(param);
    HomingTimeoutNP.setState(IPS_OK);
    HomingTimeoutNP.apply();
}

/*!
\brief Over Shoot callback.

This function gets called if Over Shoot was received from the device.
Corresponding client property will be set accordingly.

@param param Over Shoot (ticks).
*/
void BullsEyeFocuser::retOverShoot(const uint16_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retOverShoot(%d)", param);
    OverShootNP[0].setValue(param);
    OverShootNP.setState(IPS_OK);
    OverShootNP.apply();
}

/*!
\brief RMS Current callback.

This function gets called if RMS Current was received from the device.
Corresponding client property will be set accordingly.

@param param RMS Current (mA).
*/
void BullsEyeFocuser::retRmsCurrent(const uint16_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retRmsCurrent(%d)", param);
    RmsCurrentNP[0].setValue(param);
    RmsCurrentNP.setState(IPS_OK);
    RmsCurrentNP.apply();
}

/*!
\brief Flags callback.

This function gets called if Flags was received from the device.
Corresponding client properties will be set accordingly.

@param param Flags.
*/
void BullsEyeFocuser::retFlags(const uint8_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retFlags(%d)", param);
    if(param & FLAG_USEAUTOHOME)
    {
        UseAutoHomeSP[0].s = ISS_ON;
        UseAutoHomeSP[1].s = ISS_OFF;
    }
    else
    {
        UseAutoHomeSP[0].s = ISS_OFF;
        UseAutoHomeSP[1].s = ISS_ON;
    }
    UseAutoHomeSP.setState(IPS_OK);
    UseAutoHomeSP.apply();

    if(param & FLAG_INVERTDIR)
    {
        InvertDirSP[0].s = ISS_ON;
        InvertDirSP[1].s = ISS_OFF;
    }
    else
    {
        InvertDirSP[0].s = ISS_OFF;
        InvertDirSP[1].s = ISS_ON;
    }
    InvertDirSP.setState(IPS_OK);
    InvertDirSP.apply();

    if(param & FLAG_USEACCEL)
    {
        UseAccelerationSP[0].s = ISS_ON;
        UseAccelerationSP[1].s = ISS_OFF;
    }
    else
    {
        UseAccelerationSP[0].s = ISS_OFF;
        UseAccelerationSP[1].s = ISS_ON;
    }
    UseAccelerationSP.setState(IPS_OK);
    UseAccelerationSP.apply();

    m_flags = param;
}

/*!
\brief PowerStatus callback.

This function gets called if PowerStatus was received from the device.
Corresponding client property will be set accordingly.

@param param PowerStatus.
*/
void BullsEyeFocuser::retPowerStatus(const uint32_t &param)
{
    // DEBUGF(INDI::Logger::DBG_SESSION, "retPowerStatus(%d)", param);

    float volts = (float)param / 1000.0;
    PowerStatusNP[0].setValue(volts);
    PowerStatusNP.setState(IPS_OK);
    PowerStatusNP.apply();
}

/*!
\brief TmcDiag callback.

This function gets called if TmcDiag was received from the device.
Corresponding client property will be set accordingly.
*/
void BullsEyeFocuser::retTmcDiag()
{
    DEBUG(INDI::Logger::DBG_WARNING, "Stall detection triggered! Motor may have skipped steps.");
}