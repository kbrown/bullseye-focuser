/*******************************************************************************
Copyright(c) 2021 Kari Brown. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*******************************************************************************/

#include "indi_bullseyefocuser.h"
#include "indi_bullseyefocuser_callbacks.hpp"

/*! \brief CB_error boiler plate. */
void CB_error::run(const uint16_t &param)
{
    m_parent->error(param);
}

/*! \brief CB_retDeviceId boiler plate. */
void CB_retDeviceId::run(const uint32_t &param)
{
    m_parent->retDeviceId(param);
}

/*! \brief CB_retCurPosition boiler plate. */
void CB_retCurPosition::run(const uint32_t &param)
{
    m_parent->retCurPosition(param);
}

/*! \brief CB_retCurTarget boiler plate. */
void CB_retCurTarget::run(const uint32_t &param)
{
    m_parent->retCurTarget(param);
}

/*! \brief CB_retMotorStatus boiler plate. */
void CB_retMotorStatus::run(const int8_t &param)
{
    m_parent->retMotorStatus(param);
}

/*! \brief CB_retMicroSteps boiler plate. */
void CB_retMicroSteps::run(const uint8_t &param)
{
    m_parent->retMicroSteps(param);
}

/*! \brief CB_retMaxPosition boiler plate. */
void CB_retMaxPosition::run(const uint32_t &param)
{
    m_parent->retMaxPosition(param);
}

/*! \brief CB_retStepSpeed boiler plate. */
void CB_retStepSpeed::run(const uint16_t &param)
{
    m_parent->retStepSpeed(param);
}

/*! \brief CB_retTemperature boiler plate. */
void CB_retTemperature::run(const float &param)
{
    m_parent->retTemperature(param);
}

/*! \brief CB_minLimit boiler plate. */
void CB_minLimit::run(const uint8_t &param)
{
    m_parent->minLimit(param);
}

/*! \brief CB_maxLimit boiler plate. */
void CB_maxLimit::run(const uint8_t &param)
{
    m_parent->maxLimit(param);
}

/*! \brief CB_retDriverStatus boiler plate. */
void CB_retDriverStatus::run(const uint8_t &param)
{
    m_parent->retDriverStatus(param);
}

/*! \brief CB_retAcceleration boiler plate. */
void CB_retAcceleration::run(const uint16_t &param)
{
    m_parent->retAcceleration(param);
}

/*! \brief CB_retRunLevel boiler plate. */
void CB_retRunLevel::run(const uint8_t &param)
{
    m_parent->retRunLevel(param);
}

/*! \brief CB_retHomingSpeed boiler plate. */
void CB_retHomingSpeed::run(const uint16_t &param)
{
    m_parent->retHomingSpeed(param);
}

/*! \brief CB_retHomingStallValue boiler plate. */
void CB_retHomingStallValues::run(const uint16_t &param)
{
    m_parent->retHomingStallValues(param);
}

/*! \brief CB_retHomingTimeout boiler plate. */
void CB_retHomingTimeout::run(const uint8_t &param)
{
    m_parent->retHomingTimeout(param);
}

/*! \brief CB_retOverShoot boiler plate. */
void CB_retOverShoot::run(const uint16_t &param)
{
    m_parent->retOverShoot(param);
}

/*! \brief CB_retRmsCurrent boiler plate. */
void CB_retRmsCurrent::run(const uint16_t &param)
{
    m_parent->retRmsCurrent(param);
}

/*! \brief CB_retFlags boiler plate. */
void CB_retFlags::run(const uint8_t &param)
{
    m_parent->retFlags(param);
}

/*! \brief CB_retPowerStatus boiler plate. */
void CB_retPowerStatus::run(const uint32_t &param)
{
    m_parent->retPowerStatus(param);
}

/*! \brief CB_retTmcDiag boiler plate. */
void CB_retTmcDiag::run()
{
    m_parent->retTmcDiag();
}
