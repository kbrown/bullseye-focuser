# BullsEye Focuser

    /*****************************************************************************
    Copyright(c) 2021 Kari Brown. All rights reserved.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
    *****************************************************************************/

DIY Arduino + Trinamic TMC2209 stepper driver based telescope focuser with temperature measurement and INDI driver.

* [Main Project Web Page](http://knowhere.myqnapcloud.com/WordPress/2021/09/04/bullseye-diy-focuser/)
* [Arduino Firmware Documentation](https://kbrown.gitlab.io/bullseye-focuser/actuator/html/)
* [INDI Driver Documentation](https://kbrown.gitlab.io/bullseye-focuser/indi-bullseyefocuser/html/)

